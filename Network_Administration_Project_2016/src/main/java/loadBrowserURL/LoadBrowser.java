package loadBrowserURL;

import java.awt.Desktop;
import java.awt.event.ActionEvent;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;

/**
 * Network Management Project
 * 2016 Athens, Greece
 * National and Kapodistrian University of Athens (NKUA)
 *
 * created by:
 * @author Deligiannakis Nektarios		sdi1200030[at]di.uoa.gr
 * @author Milarokostas Christos		sdi1200110[at]di.uoa.gr
 *
 */

public class LoadBrowser {

	private static final int CONNECTION_TIMEOUT_TIME = 1000;
	private String[] ip_addresses;
	
	/**
	 * 
	 * @param ip_addresses
	 * @throws IOException
	 */
	public LoadBrowser(String[] ip_addresses) throws IOException{
		this.ip_addresses = ip_addresses;
		this.load_the_browser();
	}
	
	/**
	 * 
	 * @param url
	 * @throws IOException
	 */
	public LoadBrowser(String url) throws IOException{
		this.Open_browser(url);
	}
	
	/**
	 * 
	 * @param url
	 * @throws IOException
	 */
	private void Open_browser(String url) throws IOException{
		
    	String OS = System.getProperty("os.name").toLowerCase();
    	
    	Runtime runtime = Runtime.getRuntime();
		
    	if (OS.indexOf( "win" ) >= 0){
    		
    		//runtime.exec( "rundll32 url.dll,FileProtocolHandler " + url);
    		runtime.exec( "cmd /k start " + url);
    		
    	} else if (OS.indexOf( "mac" ) >= 0){
    		
    		runtime.exec( "open" + url);
    		
    	} else if (OS.indexOf( "nix") >=0 || OS.indexOf( "nux") >=0){
    		
    		String[] browsers = {"epiphany", "firefox", "mozilla", "konqueror",
    		                                 "netscape","opera","links","lynx"};

    		StringBuffer cmd = new StringBuffer();
    		for (int i=0; i<browsers.length; i++)
    		     cmd.append( (i==0  ? "" : " || " ) + browsers[i] +" \"" + url + "\" ");

    		runtime.exec(new String[] { "sh", "-c", cmd.toString() });
    		
    	} else {
    		System.out.println(OS);
    		
    		Desktop desktop = Desktop.getDesktop();
    		try {
    			desktop.browse(new URI(url));
    		} catch (Exception e) {
    			e.printStackTrace();
    		}
    	}
	}
	
	/**
	 * Finding the suitable IP address of current router
	 * @throws IOException
	 */
	private void load_the_browser() throws IOException{
		
		for(int i=0;i<ip_addresses.length;i++){
			System.out.println(ip_addresses[i]);
		}
		
		int code = 0;
		
		for(int i=0;i<ip_addresses.length;i++){
			
			//http://stackoverflow.com/questions/1378199/how-to-check-if-a-url-exists-or-returns-404-with-java
			System.setProperty("http.keepAlive", "false");
		    HttpURLConnection connection = null;
		    URL url = null;
		    try {
		        url = new URL("http://"+ip_addresses[i]);
		        System.out.println(url);
		        connection = (HttpURLConnection) url.openConnection();
		        connection.setRequestMethod("HEAD");
		        connection.setConnectTimeout(CONNECTION_TIMEOUT_TIME);
		        connection.setReadTimeout(CONNECTION_TIMEOUT_TIME);  		        
		        code = connection.getResponseCode();
		        System.out.println("" + code);
		        // You can determine on HTTP return code received. 200 is success.
		    } catch (MalformedURLException e) {
		        // TODO Auto-generated catch block
		        e.printStackTrace();
		    } catch (IOException e) {
		        // TODO Auto-generated catch block
		        e.printStackTrace();
		    } finally {
		        if (connection != null) {
		            connection.disconnect();
		        }
		    }
		        
		    if( code == HttpURLConnection.HTTP_OK || code == HttpURLConnection.HTTP_NOT_IMPLEMENTED){
		    	//trying to connect to router gives this response code HttpURLConnection.HTTP_NOT_IMPLEMENTED = 501
		    	this.Open_browser(url.toString());
		    	break;
		    }
		}
	    
	    
	}

}
