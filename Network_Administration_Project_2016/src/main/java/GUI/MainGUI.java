package GUI;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.SystemColor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.filechooser.FileFilter;

import project.Main;

import loadBrowserURL.LoadBrowser;

import Actions.ExitButtonAction;

import FileChooser.FileTypeChooserFilter;

import ProgressBar.ProgressBar4LoadingDatabase;

import PropertyFileReaders.ReadCommonModels;
import PropertyFileReaders.ReadPropertyFile;

import QoSGUI.ChannelOpt;
import QoSGUI.NetworkInfrastructure;
import QoSGUI.SignalOpt;

import SecurityGUI.NoneSec;
import SecurityGUI.UpdateSec;

import database.DBWiFiInfoView;
import database.DBXirrus;

import databaseGUI.IPScanner;
import databaseGUI.NetView;
import databaseGUI.WifiInfoView;
import databaseGUI.WireShark;
import databaseGUI.Xirrus;

/**
 * Network Management Project
 * 2016 Athens, Greece
 * National and Kapodistrian University of Athens (NKUA)
 *
 * created by:
 * @author Deligiannakis Nektarios		sdi1200030[at]di.uoa.gr
 * @author Milarokostas Christos		sdi1200110[at]di.uoa.gr
 *
 */

public class MainGUI {

	private static final String path = "routerModels";
	
	private JFrame frame;
	private int selection = 0;
	private JFileChooser chooser = null;
	
	private String character_font = "";
	private int font_number = 0;
	private String character_font_msg = "";
	private int font_number_msg = 0;
	
	private JComboBox comboBox = null;
	
	private String[] routerModels = null;
	private String selected_model = "";
	
	private String[] ip_addresses = null;
	
	private static boolean current_network_only_check = false;
	
	private static String SSID_only = null;
	
	private static int SYN_counter= 0;
	
	private static int SYN_overhead = 0;
	
	private static String potential_DOS_ATTACK_target = ""; 
	
	public synchronized static String get_Potential_DOS_ATTACK_target() {
		return potential_DOS_ATTACK_target;
	}

	public synchronized static void set_Potential_DOS_ATTACK_target(String potential_DOS_ATTACK_target) {
		MainGUI.potential_DOS_ATTACK_target = potential_DOS_ATTACK_target;
	}

	public synchronized static int get_SYN_counter() {
		return SYN_counter;
	}

	public synchronized static void set_SYN_counter(int sYN_counter) {
		SYN_counter = sYN_counter;
	}

	public synchronized static String getSSID_only() {
		return SSID_only;
	}

	public synchronized static void setSSID_only(String sSID_only) {
		SSID_only = sSID_only;
	}

	public synchronized static boolean isCurrent_network_only_check() {
		return current_network_only_check;
	}

	public synchronized static void setCurrent_network_only_check(boolean current_network_check) {
		MainGUI.current_network_only_check = current_network_check;
	}


	/**
	 * Create the application.
	 */
	public MainGUI() {
		//reading fro property file!!!!!!!!
		ReadPropertyFile properties = new ReadPropertyFile("config.properties");
		character_font = properties.getCharacterFont();
		font_number = properties.getFontNumber();
		character_font_msg = properties.getCharacterFontMessages();
		font_number_msg = properties.getFontNumberMessages();
		
		ReadCommonModels common_models = new ReadCommonModels("routerModels");
		this.routerModels = common_models.getAvailableMODELS();
		
		initialize_MainGUI();
	}

	/**
	 * Initialize the contents of the frame.
	 * This is for the main graphic user interface which consists of the first frame with the panels
	 */
	private void initialize_MainGUI() {
		
		frame = new JFrame();
		frame.setBounds(100, 100, 978, 616);
		frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		frame.setResizable(false);
		frame.getContentPane().setLayout(new BorderLayout());
		frame.setTitle("Network Management Project");
		
		
		frame.addWindowListener(new java.awt.event.WindowAdapter() {
		    @Override
		    public void windowClosing(java.awt.event.WindowEvent windowEvent) {
		    	
		    	int option = JOptionPane.showConfirmDialog(frame, "Are you sure to exit?","Exit", JOptionPane.YES_NO_OPTION,JOptionPane.QUESTION_MESSAGE);
		    	
		    	if (option == JOptionPane.YES_OPTION){
		    		System.out.println("exited");
		    		frame.dispose();
		    		
		    		Main.getConnectionPool().shutdown(); // shutdown connection pool.
		    		
		    		System.exit(0);
		    	}else{
		    		System.out.println("not exited");
		    	}
		    	
		    }
		});
		
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setBounds(12, 12, 952, 565);
		getFrame().getContentPane().add(tabbedPane);
		
		JPanel dataLoaderPane = new JPanel();
		tabbedPane.addTab("Load Data", null, dataLoaderPane, null);
		dataLoaderPane.setLayout(null);
		
		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(12, 12, 923, 281);
		dataLoaderPane.add(scrollPane_1);
		
		JTextArea infoLoader = new JTextArea();
		infoLoader.setBackground(UIManager.getColor("Button.background"));
		scrollPane_1.setViewportView(infoLoader);
		infoLoader.setEditable(false);
		infoLoader.setWrapStyleWord(false);
		
		
		final Font font = new Font(character_font, Font.BOLD, font_number);
		infoLoader.setFont(font);
		infoLoader.setForeground(Color.BLACK);
		
		final Font font_msg = new Font(character_font_msg, Font.BOLD, font_number_msg);
		infoLoader.setFont(font);
		infoLoader.setForeground(Color.BLACK);
		
		infoLoader.setText("\n\t\tWelcome to Network Management Project!\n\n"
				+ " \tThis is a platform that allows you to administer any network.\n"
				+ " \tThe platform allows you to monitor, optimize and secure any selected network.\n"
				+ " \tThere are options for achieving the best Quality of Service (QoS) and the highest security.\n "
				+ " \tIn the next lines there are short instructions for running the program.\n"
				+ " \tFor more info please refer to the included ReadMe. \n"
				+ "\n"
				+ " \tShort notice:"
				+ "\n"
				+ " \tUpload files to database."
				+ "\n"
				+ " \tSelect a program from the menu and hit the \"Choose File\" button to select a .csv file."
				+ "\n"
				+ " \tYou must specify the program, otherwise this will not warn you and will not work."
				+ "\n"
				+ " \tHit the \"Load to Database\" button!"
				+ "\n"
				+ " \tYou are good to go!");
		
		final JTextArea filePath2Load = new JTextArea();
		filePath2Load.setEditable(false);
		filePath2Load.setBounds(12, 383, 654, 35);
		filePath2Load.setFont(font);
		filePath2Load.setForeground(Color.BLACK);
		dataLoaderPane.add(filePath2Load);
		
		JRadioButton rdbtnXirrus = new JRadioButton("Xirrus Wi-Fi Inspector");
		rdbtnXirrus.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				selection = 1;
			}
		});
		rdbtnXirrus.setBounds(35, 301, 187, 23);
		dataLoaderPane.add(rdbtnXirrus);
		
		JRadioButton rdbtnWireshark = new JRadioButton("Wireshark");
		rdbtnWireshark.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				selection = 2;
			}
		});
		rdbtnWireshark.setBounds(259, 301, 149, 23);
		dataLoaderPane.add(rdbtnWireshark);
		
		JRadioButton rdbtnIpScanner = new JRadioButton("Advanced IP Scanner");
		rdbtnIpScanner.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				selection = 3;
			}
		});
		rdbtnIpScanner.setBounds(412, 301, 149, 23);
		dataLoaderPane.add(rdbtnIpScanner);
		
		JRadioButton rdbtnNetView = new JRadioButton("WirelessNetView");
		rdbtnNetView.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				selection = 4;
			}
		});
		rdbtnNetView.setBounds(615, 302, 149, 23);
		dataLoaderPane.add(rdbtnNetView);
		
		JRadioButton rdbtnWifiInfoView = new JRadioButton("WifiInfoView");
		rdbtnWifiInfoView.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				selection = 5;
			}
		});
		rdbtnWifiInfoView.setBounds(786, 302, 149, 23);
		dataLoaderPane.add(rdbtnWifiInfoView);
		
	    //Group the radio buttons.
	    final ButtonGroup group = new ButtonGroup();
	    group.add(rdbtnXirrus);
	    group.add(rdbtnWireshark);
	    group.add(rdbtnIpScanner);
	    group.add(rdbtnNetView);
	    group.add(rdbtnWifiInfoView);
		
	    
	    
		final JButton btnChoseFile = new JButton("Choose File");
		btnChoseFile.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				chooser = new JFileChooser();
				
				FileFilter CSVFilter = new FileTypeChooserFilter(".csv", "CSV Files");
				 
				chooser.addChoosableFileFilter(CSVFilter);
				
				chooser.setCurrentDirectory(new File("."));
				chooser.setDialogTitle("Choose File to Read...");
				
				chooser.setVisible( true );
			    chooser.setPreferredSize( new Dimension(800, 500) );
			    
			    
				//chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
				if(chooser.showOpenDialog(btnChoseFile) == JFileChooser.APPROVE_OPTION){
					
					chooser.getSelectedFile();
					System.out.println(chooser.getSelectedFile().getAbsolutePath());
					filePath2Load.setText(chooser.getSelectedFile().getAbsolutePath());
					
				}
			}
		});
		btnChoseFile.setBounds(693, 383, 239, 35);
		dataLoaderPane.add(btnChoseFile);
		
		JLabel lblNewLabel = new JLabel("Specify File to open....");
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setBounds(12, 356, 376, 15);
		lblNewLabel.setFont(font);
		lblNewLabel.setForeground(Color.BLACK);
		dataLoaderPane.add(lblNewLabel);
		
		JButton btnLoadToDatabase = new JButton("Load to Database");
		btnLoadToDatabase.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
					
					EventQueue.invokeLater(new Runnable() {
						public void run() {
							try {
								ProgressBar4LoadingDatabase loading = new ProgressBar4LoadingDatabase(selection, chooser.getSelectedFile());
								loading.getFrame().setVisible(true);
							} catch (Exception e) {
								e.printStackTrace();
							}
						}
					});
					
					group.clearSelection();
				}
		});
		
		btnLoadToDatabase.setBounds(234, 439, 222, 52);
		dataLoaderPane.add(btnLoadToDatabase);
		
		JPanel manageDatabasePane = new JPanel();
		tabbedPane.addTab("Manage Database", null, manageDatabasePane, null);
		manageDatabasePane.setLayout(null);
		
		JScrollPane scrollPane_2 = new JScrollPane();
		scrollPane_2.setBounds(47, 12, 304, 523);
		manageDatabasePane.add(scrollPane_2);
		
		JTextArea txtrInformation = new JTextArea();
		txtrInformation.setBackground(UIManager.getColor("Button.background"));
		scrollPane_2.setViewportView(txtrInformation);
		txtrInformation.setFont(font);
		txtrInformation.setForeground(Color.BLACK);
		txtrInformation.setText("information");
		txtrInformation.setText("  This is the Manage Database section.\n\n"
				+ "         From here you can see the\n"
				+ "         content of the database\n"
				+ "         for any selected program.");
		
		JButton loadXirrus = new JButton("Xirrus Wi-Fi Inspector");
		loadXirrus.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				EventQueue.invokeLater(new Runnable() {
					public void run() {
						try {
							Xirrus window = new Xirrus();
							window.getFrame().setVisible(true);
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				});
			}
		});
		loadXirrus.setBounds(415, 86, 521, 40);
		manageDatabasePane.add(loadXirrus);
		
		JButton loadIPScanner = new JButton("Advanced IP Scanner");
		loadIPScanner.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				EventQueue.invokeLater(new Runnable() {
					public void run() {
						try {
							IPScanner window = new IPScanner();
							window.getFrame().setVisible(true);
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				});
				
			}
		});
		loadIPScanner.setBounds(415, 156, 521, 40);
		manageDatabasePane.add(loadIPScanner);
		
		JButton loadWireshark = new JButton("Wireshark");
		loadWireshark.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				EventQueue.invokeLater(new Runnable() {
					public void run() {
						try {
							WireShark window = new WireShark();
							window.getFrame().setVisible(true);
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				});
			}
		});
		loadWireshark.setBounds(415, 229, 521, 40);
		manageDatabasePane.add(loadWireshark);
		
		JButton loadNetView = new JButton("WirelessNetView");
		loadNetView.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				EventQueue.invokeLater(new Runnable() {
					public void run() {
						try {
							NetView window = new NetView();
							window.getFrame().setVisible(true);
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				});
			}
		});
		loadNetView.setBounds(415, 301, 521, 40);
		manageDatabasePane.add(loadNetView);
		
		JButton loadWifiInfoView = new JButton("WifiInfoView");
		loadWifiInfoView.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				EventQueue.invokeLater(new Runnable() {
					public void run() {
						try {
							WifiInfoView window = new WifiInfoView();
							window.getFrame().setVisible(true);
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				});
			}
		});
		loadWifiInfoView.setBounds(415, 371, 521, 40);
		manageDatabasePane.add(loadWifiInfoView);
		
		
		//NETWORK ADMINISTRATION AND OPTIMIZATION PANEL
		//---------------------------------------------------
		JPanel network_administrtion = new JPanel();
		tabbedPane.addTab("Network Administration and Optimization", null, network_administrtion, null);
		network_administrtion.setLayout(null);
		
		JTabbedPane tabbedPane_admin_net = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane_admin_net.setBounds(12, 12, 947, 538);
		network_administrtion.add(tabbedPane_admin_net);
		
		JPanel qos_optimize = new JPanel();
		tabbedPane_admin_net.addTab("QoS Optimizations", null, qos_optimize, null);
		qos_optimize.setLayout(null);
		
		
		JPanel sec_opt = new JPanel();
		tabbedPane_admin_net.addTab("Security Optimizations", null, sec_opt, null);
		sec_opt.setLayout(null);
		
		
		
		
		
		
		//---------------------------------------------------
		
		
		
		JButton btnExit = new JButton("Exit");
		btnExit.setBounds(732, 486, 203, 45);
		
		JButton btnExit1 = new JButton("Exit");
		btnExit1.setBounds(732, 486, 203, 45);
		
		JButton btnExit2 = new JButton("Exit");
		btnExit2.setBounds(758, 457, 172, 42);
		
		JButton btnExit3 = new JButton("Exit");
		btnExit3.setBounds(758, 457, 172, 42);
		
		dataLoaderPane.add(btnExit);
		manageDatabasePane.add(btnExit1);
		qos_optimize.add(btnExit2);
		
		JButton btnOptimizeChannelusage = new JButton("Optimize Channel Usage");
		btnOptimizeChannelusage.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent arg0) {
				
				EventQueue.invokeLater(new Runnable() {
					public void run() {
						try {
							ChannelOpt window = new ChannelOpt();
							window.getFrame().setVisible(true);
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				});
				
				DBWiFiInfoView WIFIINFOVIEW = new DBWiFiInfoView(project.Main.getDb_credentials());	
				final String text = WIFIINFOVIEW.selectChannelUsageWifiInfoViewData_Opt(SSID_only);
				WIFIINFOVIEW.close_database_connection();
				
				//https://coderanch.com/t/339970/GUI/java/wrap-large-message-JOptionPane-showConfirmDialog
				
//		        JScrollPane scrollPane1 = new JScrollPane(new JLabel(text));
//		        scrollPane1.setPreferredSize(new Dimension(500,200));
//		        Object message = scrollPane1;
//		  
//		        JTextArea textArea = new JTextArea(text);
//		        textArea.setLineWrap(true);
//		        textArea.setWrapStyleWord(true);
//		        textArea.setMargin(new Insets(5,5,5,5));
//		        textArea.setFont(font_msg);
//		        textArea.setForeground(Color.BLACK);
//		        scrollPane1.getViewport().setView(textArea);
//		        message = scrollPane1;
//		        int retVal = JOptionPane.showConfirmDialog(null,
//		                                               message,
//		                                               "Fix the flaw!",
//		                                               JOptionPane.INFORMATION_MESSAGE);
//		        System.out.println("retVal = " + retVal);
		        
				EventQueue.invokeLater(new Runnable() {
					public void run() {
						try {
							SolutionsFrame window = new SolutionsFrame(text);
							window.getFrame().setVisible(true);
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				});
				

			}
		});
		btnOptimizeChannelusage.setBounds(515, 25, 407, 36);
		qos_optimize.add(btnOptimizeChannelusage);
		
		JTextArea textArea_OptimizeChannelusage = new JTextArea();
		textArea_OptimizeChannelusage.setBackground(UIManager.getColor("Button.background"));
		textArea_OptimizeChannelusage.setEditable(false);
		textArea_OptimizeChannelusage.setBounds(12, 25, 491, 80);
		qos_optimize.add(textArea_OptimizeChannelusage);
		textArea_OptimizeChannelusage.setFont(font);
		textArea_OptimizeChannelusage.setForeground(Color.BLACK);
		textArea_OptimizeChannelusage.setText(" This is the 'Optimize Channel Usage' section.\n"
				+ " Here you can check the best\n"
				+ " channel option for every network.");
		
		JButton btnFrequencyUsageOptimizations = new JButton("Signal Strength Optimization");
		btnFrequencyUsageOptimizations.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				EventQueue.invokeLater(new Runnable() {
					public void run() {
						try {
							SignalOpt window = new SignalOpt();
							window.getFrame().setVisible(true);
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				});

				//https://coderanch.com/t/339970/GUI/java/wrap-large-message-JOptionPane-showConfirmDialog
				
		        final String s = "In the pop up window you can see the networks\n"
		        		+ "that have low signal strength (below -70dB).\n"
		        		+ "The Network Management Project advises to inform\n"
		        		+ "the network owner to use more APs\n"
		        		+ "(or move closer to the existing APs!).\n";
//		        JScrollPane scrollPane = new JScrollPane(new JLabel(s));
//		        scrollPane.setPreferredSize(new Dimension(500,300));
//		        Object message = scrollPane;
//		  
//		        JTextArea textArea = new JTextArea(s);
//		        textArea.setLineWrap(true);
//		        textArea.setWrapStyleWord(true);
//		        textArea.setMargin(new Insets(5,5,5,5));
//		        textArea.setFont(font_msg);
//		        textArea.setForeground(Color.BLACK);
//		        scrollPane.getViewport().setView(textArea);
//		        message = scrollPane;
//		        int retVal = JOptionPane.showConfirmDialog(null,
//		                                               message,
//		                                               "Fix the flaw!",
//		                                               JOptionPane.INFORMATION_MESSAGE);
//		        System.out.println("retVal = " + retVal);
		        
				EventQueue.invokeLater(new Runnable() {
					public void run() {
						try {
							SolutionsFrame window = new SolutionsFrame(s);
							window.getFrame().setVisible(true);
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				});
				
			}
		});
		btnFrequencyUsageOptimizations.setBounds(515, 105, 407, 36);
		qos_optimize.add(btnFrequencyUsageOptimizations);
		
		JButton btnNetworkInfrastructure = new JButton("Optimization of 802.11 protocol");
		btnNetworkInfrastructure.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				EventQueue.invokeLater(new Runnable() {
					public void run() {
						try {
							NetworkInfrastructure window = new NetworkInfrastructure();
							window.getFrame().setVisible(true);
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				});
				
				//https://coderanch.com/t/339970/GUI/java/wrap-large-message-JOptionPane-showConfirmDialog
				
		        final String s = "In the pop up window you can see the networks that use\n"
		        		+ "the old obsolete 802.11g or 802.11b technologies.\n"
		        		+ "For an optimized network switch to 802.11n technology.\n"
		        		+ "If your router does not support 802.11n ask your ISP to provide you with a new one.\n"
		        		+ "For an even better performance you can upgrade to 802.11ac that use the 5Ghz band and gives you speed up to 1.5Gbps!";
//		        JScrollPane scrollPane = new JScrollPane(new JLabel(s));
//		        scrollPane.setPreferredSize(new Dimension(500,300));
//		        Object message = scrollPane;
//		  
//		        JTextArea textArea = new JTextArea(s);
//		        textArea.setLineWrap(true);
//		        textArea.setWrapStyleWord(true);
//		        textArea.setMargin(new Insets(5,5,5,5));
//		        textArea.setFont(font_msg);
//		        textArea.setForeground(Color.BLACK);
//		        scrollPane.getViewport().setView(textArea);
//		        message = scrollPane;
//		        int retVal = JOptionPane.showConfirmDialog(null,
//		                                               message,
//		                                               "Fix the flaw!",
//		                                               JOptionPane.INFORMATION_MESSAGE);
//		        System.out.println("retVal = " + retVal);
		        
				EventQueue.invokeLater(new Runnable() {
					public void run() {
						try {
							SolutionsFrame window = new SolutionsFrame(s);
							window.getFrame().setVisible(true);
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				});
				
				
			}
		});
		btnNetworkInfrastructure.setBounds(515, 194, 407, 36);
		qos_optimize.add(btnNetworkInfrastructure);
		
		JTextArea textArea_SignalUsageOptimizations = new JTextArea();
		textArea_SignalUsageOptimizations.setBackground(UIManager.getColor("Button.background"));
		textArea_SignalUsageOptimizations.setEditable(false);
		textArea_SignalUsageOptimizations.setFont(font);
		textArea_SignalUsageOptimizations.setForeground(Color.BLACK);
		textArea_SignalUsageOptimizations.setBounds(12, 105, 491, 80);
		qos_optimize.add(textArea_SignalUsageOptimizations);
		textArea_SignalUsageOptimizations.setText(" This is the 'Signal Strength Detector' section.\n"
				+ " Here you can check the\n"
				+ " signal strength for every network.");
		
		JTextArea textArea_NetworkInfrastructure = new JTextArea();
		textArea_NetworkInfrastructure.setBackground(UIManager.getColor("Button.background"));
		textArea_NetworkInfrastructure.setEditable(false);
		textArea_NetworkInfrastructure.setFont(font);
		textArea_NetworkInfrastructure.setForeground(Color.BLACK);
		textArea_NetworkInfrastructure.setBounds(12, 194, 491, 80);
		qos_optimize.add(textArea_NetworkInfrastructure);
		textArea_NetworkInfrastructure.setText(" This is the 'Network Infrastructure' section.\n"
				+ " Here you can see and optimize\n"
				+ " the 802.11 technology that every network use.");
		
		JTextArea textArea_TcpPacketsRatio = new JTextArea();
		textArea_TcpPacketsRatio.setBackground(UIManager.getColor("Button.background"));
		textArea_TcpPacketsRatio.setEditable(false);
		textArea_TcpPacketsRatio.setFont(font);
		textArea_TcpPacketsRatio.setForeground(Color.BLACK);
		textArea_TcpPacketsRatio.setBounds(12, 287, 491, 80);
		textArea_TcpPacketsRatio.setText(" Detect possible live Denial of Service attacks\n"
				+ " geneated from a device in your network!\n"
				+ " Only detectable when loading a new Wireshark .csv file!");		
		qos_optimize.add(textArea_TcpPacketsRatio);
		
		JButton btnTcpPacketsRatio = new JButton("SYN Packet flood detection");
		btnTcpPacketsRatio.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if ( SYN_counter > SYN_overhead){
					
					//http://www.symantec.com/connect/articles/hardening-tcpip-stack-syn-attacks
					//RAISING ALARM OF POSSIBLE DOS ATTACK
			        final String s = "ALARMING WARNING!\n"
			        		+ "Possible Denial of Service Attack in your Network.\n"
			        		+ "Targeted address is " + potential_DOS_ATTACK_target + "\n"
			        		+ "Enable \"SynAttackProtect\".\n"
			        		+ "Reset \"TcpMaxHalfOpen \" parameter to lower value (<1000 Recommended)\n"
			        		+ "in the Options tab.";
			        
					EventQueue.invokeLater(new Runnable() {
						public void run() {
							try {
								SolutionsFrame window = new SolutionsFrame(s);
								window.getFrame().setVisible(true);
							} catch (Exception e) {
								e.printStackTrace();
							}
						}
					});
					
					System.out.println(SYN_overhead+"    SIYNNC");
				}
				else {
		            JOptionPane.showMessageDialog(null, "No current attack is here !!!", "Completed", JOptionPane.INFORMATION_MESSAGE);
				}
			}
		});
		btnTcpPacketsRatio.setBounds(515, 278, 407, 36);
		qos_optimize.add(btnTcpPacketsRatio);
		
		sec_opt.add(btnExit3);
		
		JTextArea textArea = new JTextArea();
		textArea.setBackground(UIManager.getColor("Button.background"));
		textArea.setBounds(12, 30, 918, 80);
        textArea.setFont(font);
        textArea.setForeground(Color.BLACK);
		sec_opt.add(textArea);
		textArea.setText("This is the 'Security Check' section. Here you can\n"
				+ "check if the selected network is protected or not.");
		
		JButton btnSecurityCheckfor = new JButton("Security Check (for none security)");
		btnSecurityCheckfor.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				//set database view
				//and show joption pane
				
				//LOOK AT THIS!!!!!!!!!!!!!!!!!!!!!!!!!!!!
				//https://coderanch.com/t/339970/GUI/java/wrap-large-message-JOptionPane-showConfirmDialog
				
				EventQueue.invokeLater(new Runnable() {
					public void run() {
						try {
							NoneSec window = new NoneSec();
							window.getFrame().setVisible(true);
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				});
				
				//https://coderanch.com/t/339970/GUI/java/wrap-large-message-JOptionPane-showConfirmDialog
				
		        final String s = "In the pop-up window you can see which network connections have no security.\n"
		        		+ "For these connections you can secure the network with a WEP or WPA security.\n"
		        		+ "When you do that you will have the option to upgrade to the even better WPA2-PSK\n"
		        		+ "security using an AES/CCMP cipher.";
//		        JScrollPane scrollPane = new JScrollPane(new JLabel(s));
//		        scrollPane.setPreferredSize(new Dimension(500,300));
//		        Object message = scrollPane;
//		  
//		        JTextArea textArea = new JTextArea(s);
//		        textArea.setLineWrap(true);
//		        textArea.setWrapStyleWord(true);
//		        textArea.setMargin(new Insets(5,5,5,5));
//		        textArea.setFont(font_msg);
//		        textArea.setForeground(Color.BLACK);
//		        scrollPane.getViewport().setView(textArea);
//		        message = scrollPane;
//		        int retVal = JOptionPane.showConfirmDialog(null,
//		                                               message,
//		                                               "Fix the flaw!",
//		                                               JOptionPane.INFORMATION_MESSAGE);
//		        System.out.println("retVal = " + retVal);
		        
				EventQueue.invokeLater(new Runnable() {
					public void run() {
						try {
							SolutionsFrame window = new SolutionsFrame(s);
							window.getFrame().setVisible(true);
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				});
			}
		});
		btnSecurityCheckfor.setBounds(523, 133, 407, 36);
		sec_opt.add(btnSecurityCheckfor);
		
		JTextArea textArea_1 = new JTextArea();
		textArea_1.setBackground(UIManager.getColor("Button.background"));
		textArea_1.setBounds(12, 220, 918, 80);
        textArea_1.setFont(font);
        textArea_1.setForeground(Color.BLACK);
		sec_opt.add(textArea_1);
		textArea_1.setText("This is the 'Optimize Network Security' section. If the selected network\n"
				+ "uses an outdated security you have the option to update to WEP2-PSK.");
		
		JButton btnOptimizeSecurity = new JButton("Optimize Network Security");
		btnOptimizeSecurity.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				EventQueue.invokeLater(new Runnable() {
					public void run() {
						try {
							UpdateSec window = new UpdateSec();
							window.getFrame().setVisible(true);
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				});
				
				//https://coderanch.com/t/339970/GUI/java/wrap-large-message-JOptionPane-showConfirmDialog
				
		        final String s = "In the pop-up window you can check the security of the monitored networks.\n"
		        		+ "As you can see most of them use WEP or WPA security.\n"
		        		+ "Now you can upgrade to the new cool WPA2-PSK security\n"
		        		+ "that use the brand new cipher AES-CCMP";
//		        JScrollPane scrollPane = new JScrollPane(new JLabel(s));
//		        scrollPane.setPreferredSize(new Dimension(500,300));
//		        Object message = scrollPane;
//		  
//		        JTextArea textArea = new JTextArea(s);
//		        textArea.setLineWrap(true);
//		        textArea.setWrapStyleWord(true);
//		        textArea.setMargin(new Insets(5,5,5,5));
//		        textArea.setFont(font_msg);
//		        textArea.setForeground(Color.BLACK);
//		        scrollPane.getViewport().setView(textArea);
//		        message = scrollPane;
//		        int retVal = JOptionPane.showConfirmDialog(null,
//		                                               message,
//		                                               "Fix the flaw!",
//		                                               JOptionPane.INFORMATION_MESSAGE);
//		        System.out.println("retVal = " + retVal);
		        
				EventQueue.invokeLater(new Runnable() {
					public void run() {
						try {
							SolutionsFrame window = new SolutionsFrame(s);
							window.getFrame().setVisible(true);
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				});
				
				
			}
		});
		btnOptimizeSecurity.setBounds(523, 326, 407, 36);
		sec_opt.add(btnOptimizeSecurity);
		
		JPanel panel_1 = new JPanel();
		tabbedPane_admin_net.addTab("User", null, panel_1, null);
		panel_1.setLayout(null);
		
		JPanel panel_User = new JPanel();
		panel_User.setLayout(null);
		panel_User.setBounds(0, 0, 942, 508);
		panel_1.add(panel_User);
		
		comboBox = new JComboBox(this.routerModels);
		
		comboBox.setForeground(Color.BLUE);
		comboBox.setFont(new Font("Arial", Font.BOLD, 14));
		comboBox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				selected_model = (String) comboBox.getSelectedItem();
				ReadCommonModels read = new ReadCommonModels(path);
				ip_addresses = read.getAvailableIPs(selected_model);
				try {
					new LoadBrowser(ip_addresses);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		
		comboBox.setForeground(Color.BLUE);
		comboBox.setFont(new Font("Arial", Font.BOLD, 14));
		comboBox.setBounds(318, 171, 272, 33);
		panel_User.add(comboBox);
		
		JLabel label = new JLabel("Choose your modem model below :");
		label.setBounds(318, 110, 327, 31);
		panel_User.add(label);
		
		JButton button_2 = new JButton("Help me");
		button_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					new LoadBrowser("http://www.techspot.com/guides/287-default-router-ip-addresses/");
				} catch (IOException ex) {
					// TODO Auto-generated catch block
					ex.printStackTrace();
				}
			}
		});
		button_2.setBounds(403, 310, 97, 25);
		panel_User.add(button_2);
		
		JLabel label_1 = new JLabel("If your router model is not listed or you dont know it click below for help!");
		label_1.setBounds(246, 241, 433, 31);
		panel_User.add(label_1);
		
		JButton button_3 = new JButton("Exit");
		button_3.addActionListener(new ExitButtonAction(frame));
		button_3.setBounds(727, 450, 203, 45);
		panel_User.add(button_3);
		
		JPanel panel_2 = new JPanel();
		tabbedPane_admin_net.addTab("Options", null, panel_2, null);
		panel_2.setLayout(null);
		
		JPanel panel_3 = new JPanel();
		panel_3.setLayout(null);
		panel_3.setBounds(0, 0, 942, 508);
		panel_2.add(panel_3);
		
		JTextArea txtrChooseWeatherYo = new JTextArea();
		txtrChooseWeatherYo.setEditable(false);
		txtrChooseWeatherYo.setFont(font);
		txtrChooseWeatherYo.setBackground(UIManager.getColor("Button.background"));
		txtrChooseWeatherYo.setText("Choose whether you want to optimize only the network\r\nyou are connected to or not.");
		txtrChooseWeatherYo.setFont(font);
		txtrChooseWeatherYo.setBackground(SystemColor.menu);
		txtrChooseWeatherYo.setBounds(38, 30, 491, 53);
		panel_3.add(txtrChooseWeatherYo);
		
		final JRadioButton radioButton = new JRadioButton("Connected Network");
		radioButton.setBounds(537, 30, 166, 25);
		radioButton.setSelected(false);
		panel_3.add(radioButton);
		
		final JRadioButton radioButton_1 = new JRadioButton("All networks");
		radioButton_1.setBounds(718, 30, 127, 25);
		radioButton_1.setSelected(true);
		panel_3.add(radioButton_1);
		
		radioButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				current_network_only_check = true;
				radioButton.setSelected(true);
				radioButton_1.setSelected(false);
				
				DBXirrus xirrus = new DBXirrus(project.Main.getDb_credentials());
				SSID_only = xirrus.select_Xirrus_connected_network();
				xirrus.close_database_connection();
				
				System.out.println(SSID_only);
				if(SSID_only == null){
		            JOptionPane.showMessageDialog(null,"We cant find your network!\n"
		            		+ "You need to update information on database.", "No Network Found.", JOptionPane.INFORMATION_MESSAGE);
		            radioButton.setSelected(false);
		            radioButton_1.setSelected(true);
				}
			}
		});
		
		radioButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				current_network_only_check = false;
				radioButton.setSelected(false);
				radioButton_1.setSelected(true);
				SSID_only = null;
			}
		});
		
		JButton button = new JButton("Exit");
		button.addActionListener(new ExitButtonAction(frame));
		button.setBounds(727, 450, 203, 45);
		panel_3.add(button);
		
		JTextArea textArea_3 = new JTextArea();
		textArea_3.setText("Handle the number of incoming connections\n"
				+ "you are willing to allow before a possible DOS Attack\n"
				+ "is present.\n"
				+ "(Number of SYN RECEIVED state)");
		textArea_3.setFont(font);
		textArea_3.setBackground(UIManager.getColor("Button.background"));
		textArea_3.setBounds(38, 96, 491, 53);
		panel_3.add(textArea_3);
		
		String[] values = { "500", "1000", "2000", "3000" };
		
		final JComboBox comboBox_1 = new JComboBox(values);
		comboBox_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String val = (String) comboBox_1.getSelectedItem();
				SYN_overhead = Integer.parseInt(val);
			}
		});
		comboBox_1.setBounds(616, 95, 166, 25);
		panel_3.add(comboBox_1);
		
		btnExit.addActionListener(new ExitButtonAction(frame));
		btnExit1.addActionListener(new ExitButtonAction(frame));
		btnExit2.addActionListener(new ExitButtonAction(frame));
		btnExit3.addActionListener(new ExitButtonAction(frame));
		
	}

	public JFrame getFrame() {
		return frame;
	}

	public void setFrame(JFrame frame) {
		this.frame = frame;
	}
}
