package GUI;

import java.awt.Color;
import java.awt.Font;

import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.JTextArea;
import javax.swing.JScrollPane;

import Actions.ExitButtonAction_JustDispose;

/**
 * Network Management Project
 * 2016 Athens, Greece
 * National and Kapodistrian University of Athens (NKUA)
 *
 * created by:
 * @author Deligiannakis Nektarios		sdi1200030[at]di.uoa.gr
 * @author Milarokostas Christos		sdi1200110[at]di.uoa.gr
 *
 */

public class SolutionsFrame {

	private JFrame frame;
	private String textToPrint;

	/**
	 * Create the application.
	 */
	public SolutionsFrame(String textToPrint) {
		this.textToPrint = textToPrint;
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 615, 381);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		frame.getContentPane().setLayout(null);
		frame.setResizable(false);
		frame.setTitle("Solution Report!");
		
		JButton btnClose = new JButton("Close");
		btnClose.addActionListener(new ExitButtonAction_JustDispose(frame));
		btnClose.setBounds(500, 308, 97, 25);
		frame.getContentPane().add(btnClose);
		Font font = new Font("Verdana", Font.BOLD, 12);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(12, 13, 585, 279);
		frame.getContentPane().add(scrollPane);
		
		JTextArea textArea = new JTextArea(this.textToPrint);
		textArea.setEditable(false);
		scrollPane.setViewportView(textArea);
		textArea.setFont(font);
		textArea.setForeground(Color.BLACK);
	}

	public JFrame getFrame() {
		return frame;
	}

	public void setFrame(JFrame frame) {
		this.frame = frame;
	}
}
