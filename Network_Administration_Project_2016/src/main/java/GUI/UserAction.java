package GUI;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import PropertyFileReaders.ReadCommonModels;
import loadBrowserURL.LoadBrowser;

/**
 * Network Management Project
 * 2016 Athens, Greece
 * National and Kapodistrian University of Athens (NKUA)
 *
 * created by:
 * @author Deligiannakis Nektarios		sdi1200030[at]di.uoa.gr
 * @author Milarokostas Christos		sdi1200110[at]di.uoa.gr
 *
 */

public class UserAction implements ActionListener {
	
	final static private String path = "routerModels";
	private String model;
	
	/**
	 * 
	 * @param model
	 */
	public UserAction(String model){
		this.model = model;
		System.out.println("in model" + model);
	}

	public void actionPerformed(ActionEvent e) {
		
		ReadCommonModels read = new ReadCommonModels(this.path);
		
		String[] ip_addresses = read.getAvailableIPs(model);
		
		try {
			LoadBrowser browser = new LoadBrowser(ip_addresses);
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}


}
