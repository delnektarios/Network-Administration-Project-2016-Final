package Actions;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;

/**
 * Network Management Project
 * 2016 Athens, Greece
 * National and Kapodistrian University of Athens (NKUA)
 *
 * created by:
 * @author Deligiannakis Nektarios		sdi1200030[at]di.uoa.gr
 * @author Milarokostas Christos		sdi1200110[at]di.uoa.gr
 *
 */

public class ExitButtonAction_JustDispose implements ActionListener {

	private JFrame frame;
	
	/**
	 * 
	 * @param frame
	 */
	public ExitButtonAction_JustDispose(JFrame frame){
		this.frame = frame;
	}
	/**
	 * this is the action dispose the frame
	 */
	public void actionPerformed(ActionEvent e) {

		frame.dispose();

	}

}
