package databaseGUI;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import Actions.ExitButtonAction_JustDispose;
import GUI.MainGUI;
import database.DBWiFiInfoView;

/**
 * Network Management Project
 * 2016 Athens, Greece
 * National and Kapodistrian University of Athens (NKUA)
 *
 * created by:
 * @author Deligiannakis Nektarios		sdi1200030[at]di.uoa.gr
 * @author Milarokostas Christos		sdi1200110[at]di.uoa.gr
 *
 */
public class WifiInfoView {

	private JFrame frame;
	private JTable WifiInfoViewTableData;

	/**
	 * Create the application.
	 */
	public WifiInfoView() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		
		frame = new JFrame();
		frame.setBounds(100, 100, 1056, 660);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		frame.setLocationRelativeTo(null);
		frame.setTitle("WifiInfoView Data Table");
		frame.setResizable(false);
		
		//HERE TO SET DATA FROM DATABASE!!!
		
		DBWiFiInfoView wifiinfoview = new DBWiFiInfoView(project.Main.getDb_credentials());
		DefaultTableModel model = wifiinfoview.selectWifiInfoViewData(MainGUI.getSSID_only());
		wifiinfoview.close_database_connection();
		
		if(model == null){
            JOptionPane.showMessageDialog(null, "Something  went wrong!\n"
            		+ "Posibly empty database!", "Completed", JOptionPane.INFORMATION_MESSAGE);
            frame.dispose();
		}
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(12, 12, 1030, 518);
		frame.getContentPane().add(scrollPane);
		
		WifiInfoViewTableData = new JTable();
		WifiInfoViewTableData.setCellSelectionEnabled(true);
		WifiInfoViewTableData.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		
		WifiInfoViewTableData.setModel(model);
		scrollPane.setViewportView(WifiInfoViewTableData);
		
		
		JButton btnClose = new JButton("Close");
		btnClose.setBounds(841, 561, 201, 43);
		frame.getContentPane().add(btnClose);
		btnClose.addActionListener(new ExitButtonAction_JustDispose(frame));
		
		
	}

	public JFrame getFrame() {
		return frame;
	}

	public void setFrame(JFrame frame) {
		this.frame = frame;
	}

}
