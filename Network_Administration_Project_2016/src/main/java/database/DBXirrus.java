package database;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.table.DefaultTableModel;

/**
 * Network Management Project
 * 2016 Athens, Greece
 * National and Kapodistrian University of Athens (NKUA)
 *
 * created by:
 * @author Deligiannakis Nektarios		sdi1200030[at]di.uoa.gr
 * @author Milarokostas Christos		sdi1200110[at]di.uoa.gr
 *
 */
public class DBXirrus extends Database {
	
	public DBXirrus(DBCredentials_and_ConnectionPool cred) {
		super(cred);
	}
	
	/**
	 * 
	 * @param SSID
	 * @param Connected
	 * @param Signal
	 * @param Network_Mode
	 * @param Default_Encryption
	 * @param Default_Auth
	 * @param Vendor
	 * @param BSSID
	 * @param Channel
	 * @param Frequency
	 * @param Network_Type
	 * @param Adapter_Description
	 * @return 0 on success
	 */
	public int insert_DBXirrus_stats (String SSID,String Connected,String Signal,String Network_Mode,
			String Default_Encryption,String Default_Auth,String Vendor,String BSSID,String Channel,
			String Frequency,String Network_Type,String Adapter_Description){
		
		Statement stmt = null;
		System.out.println("Creating statement...");
		try {
			stmt = conn.createStatement();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		int BOOL_Connected = 0;
		System.out.println(Connected);
		if(Connected.equals("True")){
			BOOL_Connected = 1;
		}else{
			BOOL_Connected  = 0;
		}

		try {	
			
			//http://stackoverflow.com/questions/5005388/cannot-add-or-update-a-child-row-a-foreign-key-constraint-fails
			stmt.executeQuery("SET FOREIGN_KEY_CHECKS=0");
			
			//stmt.executeUpdate("INSERT INTO Xirrus"
			stmt.executeUpdate("INSERT INTO Xirrus (Connected,SSID,Signal_Strength,Network_Mode,Default_Encryption,Default_Auth,Vendor,BSSID,Channel,Frequency,Network_Type,Adapter_Description)"
					+ " VALUES('" 
					+ BOOL_Connected
					+ "','"
					+ SSID
					+ "','"
					+ Signal
					+ "','"
					+ Network_Mode
					+ "','"
					+ Default_Encryption
					+ "','"
					+ Default_Auth
					+ "','"
					+ Vendor
					+ "','"
					+ BSSID
					+ "','"
					+ Channel
					+ "','"
					+ Frequency
					+ "','"
					+ Network_Type
					+ "','"
					+ Adapter_Description + "');");		
			
			//http://stackoverflow.com/questions/5005388/cannot-add-or-update-a-child-row-a-foreign-key-constraint-fails
			stmt.executeQuery("SET FOREIGN_KEY_CHECKS=1");
			
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return 0;
	}
	
	/**
	 * 
	 * @param SSID
	 * @return DefaultTableModel model to set to JTable 
	 */
	public DefaultTableModel selectXirrusData(String SSID){
		
		Statement stmt = null;
		ResultSet results = null;
		
		DefaultTableModel model = new DefaultTableModel();
		
		System.out.println("Creating statement...");
		try {
			stmt = conn.createStatement();
		} catch (SQLException e) {
			e.printStackTrace();
		}
				
		try {
			results = stmt.executeQuery("SELECT `Xirrus`.`Connected`, `Xirrus`.`SSID`,"
					+ "`Xirrus`.`Signal_Strength`,`Xirrus`.`Network_Mode`, "
					+ "`Xirrus`.`Default_Encryption`,`Xirrus`.`Default_Auth`,"
					+ "`Xirrus`.`Vendor`,`Xirrus`.`BSSID`,"
					+ "`Xirrus`.`Channel`,`Xirrus`.`Frequency`,"
					+ "`Xirrus`.`Network_Type`,`Xirrus`.`Adapter_Description`"
					+ "FROM `mydb`.`Xirrus` ");

		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		if (results == null) {
			return null;
		}		
		
		try {
			
	        int columns = results.getMetaData().getColumnCount();
	        System.out.println(columns);
	        
	        Object[] obj = new Object[columns];
	        
	        for (int i=0;i<columns;i++){
	        	obj[i] = results.getMetaData().getColumnLabel(i+1);
	        }
	        
	        model.setColumnIdentifiers(obj);
	        
			while (results.next()) {
		        
	            Object[] row = new Object[columns];
	            for (int i=0; i<model.getColumnCount(); i++)
	            {  
	                row[i] = results.getObject(i+1);
	            }
	            
	            model.addRow(row);

			}
			results.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		if (model.getRowCount() < 2) {
			System.out.println("empty set");
			return null;
		}

		return model;
		
	}
	
	/**
	 * 
	 * @param SSID
	 * @return DefaultTableModel model to set to JTable
	 */
	public DefaultTableModel selectXirrusSignalData(String SSID){
		
		Statement stmt = null;
		ResultSet results = null;
		
		DefaultTableModel model = new DefaultTableModel();
		
		System.out.println("Creating statement...");
		try {
			stmt = conn.createStatement();
		} catch (SQLException e) {
			e.printStackTrace();
		}
				
		//select Xirrus.SSID, Xirrus.Network_Mode, Xirrus.BSSID, Xirrus.Network_Type from mydb.Xirrus where Signal_Strength < -70; 
		try {
			
			
			if(SSID == null){
				results = stmt.executeQuery("SELECT `Xirrus`.`SSID`,"
						+ "`Xirrus`.`Network_Mode`, "
						+ "`Xirrus`.`BSSID`, "
						+ "`Xirrus`.`Channel`, "
						+ "`Xirrus`.`Network_Type`,`Xirrus`.`Signal_Strength` "
						+ "FROM `mydb`.`Xirrus`"
						+ "WHERE `Xirrus`.`Signal_Strength` < -70 ");
			} else {
				results = stmt.executeQuery("SELECT `Xirrus`.`SSID`,"
						+ "`Xirrus`.`Network_Mode`, "
						+ "`Xirrus`.`BSSID`, "
						+ "`Xirrus`.`Channel`, "
						+ "`Xirrus`.`Network_Type`,`Xirrus`.`Signal_Strength` "
						+ "FROM `mydb`.`Xirrus`"
						+ "WHERE `Xirrus`.`Signal_Strength` < -70 AND `NetView`.`SSID` = '" + SSID + "'");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		if (results == null) {
			return null;
		}		
		
		try {
			
	        int columns = results.getMetaData().getColumnCount();
	        System.out.println(columns);
	        
	        Object[] obj = new Object[columns];
	        
	        for (int i=0;i<columns;i++){
	        	obj[i] = results.getMetaData().getColumnLabel(i+1);
	        }
	        
	        model.setColumnIdentifiers(obj);
	        
			while (results.next()) {
		        
	            Object[] row = new Object[columns];
	            for (int i=0; i<model.getColumnCount(); i++)
	            {  
	                row[i] = results.getObject(i+1);
	            }
	            
	            model.addRow(row);
			}
			results.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		if (model.getRowCount() < 2) {
			System.out.println("empty set");
			return null;
		}

		return model;
		
	}
	
	/**
	 * 
	 * @return the SSID of the network we are currently connected to
	 */
	public String select_Xirrus_connected_network(){
		Statement stmt = null;
		ResultSet results = null;
		
		String SSID = null;
		
		System.out.println("Creating statement...");
		try {
			stmt = conn.createStatement();
		} catch (SQLException e) {
			e.printStackTrace();
		}
				
		try {
//			SELECT xirrus.SSID
//			FROM mydb.xirrus
//			where xirrus.Connected = 1
//			order by xirrus.No desc
//			limit 1;
			results = stmt.executeQuery("SELECT `Xirrus`.`SSID` "
							+ "FROM `mydb`.`Xirrus` "
							+ "WHERE `Xirrus`.`Connected` = 1 "
							+ "ORDER BY `Xirrus`.`No` DESC "
							+ "LIMIT 1 ");
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		if (results == null) {
			return SSID;
		}		
		
		try {
			while (results.next()) {
				SSID = results.getString("SSID");
			}
			results.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return SSID;
		
	}
	
	/**
	 * 
	 * @param SSID
	 * @return DefaultTableModel model to set to JTable
	 */
	public DefaultTableModel selectXirrusSignalData_connected_network(String SSID){
		
		Statement stmt = null;
		ResultSet results = null;
		
		DefaultTableModel model = new DefaultTableModel();
		
		System.out.println("Creating statement...");
		try {
			stmt = conn.createStatement();
		} catch (SQLException e) {
			e.printStackTrace();
		}
				
		//select Xirrus.SSID, Xirrus.Network_Mode, Xirrus.BSSID, Xirrus.Network_Type from mydb.Xirrus where Signal_Strength < -70; 
		try {
			results = stmt.executeQuery("SELECT `Xirrus`.`SSID`,"
							+ "`Xirrus`.`Network_Mode`, "
							+ "`Xirrus`.`BSSID`, "
							+ "`Xirrus`.`Channel`, "
							+ "`Xirrus`.`Network_Type`,`Xirrus`.`Signal_Strength` "
							+ "FROM `mydb`.`Xirrus`"
							+ "WHERE `Xirrus`.`Signal_Strength` < -70 ");
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		if (results == null) {
			return null;
		}		
		
		try {
			
	        int columns = results.getMetaData().getColumnCount();
	        System.out.println(columns);
	        
	        Object[] obj = new Object[columns];
	        
	        for (int i=0;i<columns;i++){
	        	obj[i] = results.getMetaData().getColumnLabel(i+1);
	        }
	        
	        model.setColumnIdentifiers(obj);
	        
			while (results.next()) {
		        
	            Object[] row = new Object[columns];
	            for (int i=0; i<model.getColumnCount(); i++)
	            {  
	                row[i] = results.getObject(i+1);
	            }
	            
	            model.addRow(row);
			}
			results.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		if (model.getRowCount() < 2) {
			System.out.println("empty set");
			return null;
		}

		return model;
		
	}

}
