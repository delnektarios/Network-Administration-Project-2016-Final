package database;

import java.sql.Connection;

import java.sql.SQLException;

import com.jolbox.bonecp.BoneCP;

/**
 * Network Management Project
 * 2016 Athens, Greece
 * National and Kapodistrian University of Athens (NKUA)
 *
 * created by:
 * @author Deligiannakis Nektarios		sdi1200030[at]di.uoa.gr
 * @author Milarokostas Christos		sdi1200110[at]di.uoa.gr
 *
 */
public class Database {
	
	final String DB_URL;
	final String USER;
	final String PASSWORD;
	protected Connection conn ;
	
	/**
	 * 
	 * @param cred
	 */
	public Database(DBCredentials_and_ConnectionPool cred) {
		this(cred.url, cred.username, cred.password, cred.connectionPool);
	}
	
	/**
	 * 
	 * @param db_url
	 * @param user
	 * @param password
	 * @param connectionPool
	 */
	public Database(String db_url,String user,String password,BoneCP connectionPool){

		DB_URL=db_url;
		USER=user;
		PASSWORD=password;
		
		System.out.printf("Connecting to database %s ",DB_URL+"\n");
		try {
			conn = connectionPool.getConnection();
		} catch (SQLException e) {
			e.printStackTrace();
			System.exit(-1);
		}
		System.out.printf("Connected to database %s ",DB_URL);
	}
	
	/**
	 * 
	 * @return 0 on successfully connection closure
	 */
	public int close_database_connection(){
		try {
			conn.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return 0;
	}

}
