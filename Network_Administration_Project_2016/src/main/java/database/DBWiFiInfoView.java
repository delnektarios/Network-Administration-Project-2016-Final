package database;

import integer_pairs.IntPair;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.swing.table.DefaultTableModel;

/**
 * Network Management Project
 * 2016 Athens, Greece
 * National and Kapodistrian University of Athens (NKUA)
 *
 * created by:
 * @author Deligiannakis Nektarios		sdi1200030[at]di.uoa.gr
 * @author Milarokostas Christos		sdi1200110[at]di.uoa.gr
 *
 */
public class DBWiFiInfoView extends Database {

	/**
	 * 
	 * @param cred
	 */
	public DBWiFiInfoView(DBCredentials_and_ConnectionPool cred) {
		super(cred);
	}
	
	/**
	 * 
	 * @param SSID
	 * @param MAC_Address
	 * @param PHY_Type
	 * @param RSSI
	 * @param Signal_Quality
	 * @param Channel_Frequency
	 * @param Channel
	 * @param Information_Size
	 * @param Elements_Count
	 * @param Company
	 * @param Router_Model
	 * @param Router_Name
	 * @param Security
	 * @param Cipher
	 * @param Maximum_Speed
	 * @param Frequency
	 * @param First_Detection
	 * @param Last_Detection
	 * @param Detection_Count
	 * @return 0 on success
	 */
	public int insert_DBWiFiInfoView_stats (String SSID,String MAC_Address,
			String PHY_Type,String RSSI,
			String Signal_Quality,String Channel_Frequency,String Channel,
			String Information_Size,String Elements_Count,String Company,
			String Router_Model,String Router_Name,String Security,
			String Cipher,String Maximum_Speed,String Frequency,String First_Detection,
			String Last_Detection,String Detection_Count){
		
		Statement stmt = null;
		System.out.println("Creating statement...");
		try {
			stmt = conn.createStatement();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		try {		
			
			//http://stackoverflow.com/questions/5005388/cannot-add-or-update-a-child-row-a-foreign-key-constraint-fails
			stmt.executeQuery("SET FOREIGN_KEY_CHECKS=0");
			
			stmt.executeUpdate("INSERT INTO WiFiInfoView(SSID,MAC_Address,PHY_Type,RSSI,Signal_Quality,Channel_Frequency,Channel,"
					+ "Information_Size,Elements_Count,Company,Router_Model,Router_Name,Security,Cipher,Maximum_Speed,Frequency,"
					+ "First_Detection,Last_Detection,Detection_Count) VALUE('"
					+ SSID
					+ "','"
					+ MAC_Address
					+ "','"
					+ PHY_Type
					+ "','"
					+ RSSI
					+ "','"
					+ Signal_Quality
					+ "','"
					+ Channel_Frequency
					+ "','"
					+ Channel 
					+ "','"
					+ Information_Size 
					+ "','"
					+ Elements_Count  
					+ "','"
					+ Company 
					+ "','"
					+ Router_Model 
					+ "','"
					+ Router_Name 
					+ "','"
					+ Security 
					+ "','"
					+ Cipher 
					+ "','"
					+ Maximum_Speed 
					+ "','"
					+ Frequency 
					+ "','"
					+ First_Detection 
					+ "','"
					+ Last_Detection 
					+ "','"
					+ Detection_Count + "')");	
			
			stmt.executeQuery("SET FOREIGN_KEY_CHECKS=1");
			
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return 0;
	}
	
	/**
	 * 
	 * @param SSID
	 * @return DefaultTableModel model to set to JTable
	 */
	public DefaultTableModel selectWifiInfoViewData(String SSID){
		
		Statement stmt = null;
		ResultSet results = null;
		
		DefaultTableModel model = new DefaultTableModel();
		
		System.out.println("Creating statement...");
		try {
			stmt = conn.createStatement();
		} catch (SQLException e) {
			e.printStackTrace();
		}
				
				
		try {
			if (SSID == null){
				results = stmt.executeQuery("SELECT `WiFiInfoView`.`SSID`, `WiFiInfoView`.`MAC_Address`,"
						+ "`WiFiInfoView`.`PHY_Type`,`WiFiInfoView`.`RSSI`, "
						+ "`WiFiInfoView`.`Signal_Quality`,`WiFiInfoView`.`Channel_Frequency`,"
						+ "`WiFiInfoView`.`Channel`,`WiFiInfoView`.`Information_Size`, "
						+ "`WiFiInfoView`.`Elements_Count`,`WiFiInfoView`.`Company`,`WiFiInfoView`.`Router_Model`,"
						+ "`WiFiInfoView`.`Router_Name`, "
						+ "`WiFiInfoView`.`Security`,`WiFiInfoView`.`Cipher`,"
						+ "`WiFiInfoView`.`Maximum_Speed`,`WiFiInfoView`.`Frequency`, "
						+ "`WiFiInfoView`.`First_Detection`,`WiFiInfoView`.`Last_Detection`,"
						+ "`WiFiInfoView`.`Detection_Count` "
						+ "FROM `mydb`.`WiFiInfoView` ");
			} else {
				results = stmt.executeQuery("SELECT `WiFiInfoView`.`SSID`, `WiFiInfoView`.`MAC_Address`,"
						+ "`WiFiInfoView`.`PHY_Type`,`WiFiInfoView`.`RSSI`, "
						+ "`WiFiInfoView`.`Signal_Quality`,`WiFiInfoView`.`Channel_Frequency`,"
						+ "`WiFiInfoView`.`Channel`,`WiFiInfoView`.`Information_Size`, "
						+ "`WiFiInfoView`.`Elements_Count`,`WiFiInfoView`.`Company`,`WiFiInfoView`.`Router_Model`,"
						+ "`WiFiInfoView`.`Router_Name`, "
						+ "`WiFiInfoView`.`Security`,`WiFiInfoView`.`Cipher`,"
						+ "`WiFiInfoView`.`Maximum_Speed`,`WiFiInfoView`.`Frequency`, "
						+ "`WiFiInfoView`.`First_Detection`,`WiFiInfoView`.`Last_Detection`,"
						+ "`WiFiInfoView`.`Detection_Count` "
						+ "FROM `mydb`.`WiFiInfoView` "
						+ "`WiFiInfoView`.`SSID` = '" + SSID + "'");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		if (results == null) {
			return null;
		}		
		
		try {
			
	        int columns = results.getMetaData().getColumnCount();
	        System.out.println(columns);
	        
	        Object[] obj = new Object[columns];
	        
	        for (int i=0;i<columns;i++){
	        	obj[i] = results.getMetaData().getColumnLabel(i+1);
	        }
	        
	        model.setColumnIdentifiers(obj);
	        
			while (results.next()) {
		        
	            Object[] row = new Object[columns];
	            for (int i=0; i<model.getColumnCount(); i++)
	            {  
	                row[i] = results.getObject(i+1);
	            }
	            
	            model.addRow(row);

			}
			results.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		if (model.getRowCount() < 2) {
			System.out.println("empty set");
			return null;
		}

		return model;
		
	}
	
	/**
	 * 
	 * @param SSID
	 * @return DefaultTableModel model to set to JTable
	 */
	public DefaultTableModel selectSecurityNoneWifiInfoViewData(String SSID){
		
		Statement stmt = null;
		ResultSet results = null;
		
		DefaultTableModel model = new DefaultTableModel();
		
		System.out.println("Creating statement...");
		try {
			stmt = conn.createStatement();
		} catch (SQLException e) {
			e.printStackTrace();
		}
			
		
		//select WiFiInfoView.SSID, WiFiInfoView.Security, WiFiInfoView.Cipher from mydb.WiFiInfoView where (WiFiInfoView.Security = "none" or WiFiInfoView.Security = "");
				
		try {
			if (SSID == null){
				results = stmt.executeQuery("SELECT `WiFiInfoView`.`SSID`, `WiFiInfoView`.`Security`,"
						+ "`WiFiInfoView`.`Cipher` "
						+ "FROM `mydb`.`WiFiInfoView` "
						+ "where (WiFiInfoView.Security = \"none\" or WiFiInfoView.Security = \"\") ");
			} else {
				results = stmt.executeQuery("SELECT `WiFiInfoView`.`SSID`, `WiFiInfoView`.`Security`,"
						+ "`WiFiInfoView`.`Cipher` "
						+ "FROM `mydb`.`WiFiInfoView` "
						+ "WHERE (WiFiInfoView.Security = \"none\" or WiFiInfoView.Security = \"\") AND `WiFiInfoView`.`SSID` = '" + SSID + "'");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		if (results == null) {
			return null;
		}		
		
		try {
			
	        int columns = results.getMetaData().getColumnCount();
	        System.out.println(columns);
	        
	        Object[] obj = new Object[columns];
	        
	        for (int i=0;i<columns;i++){
	        	obj[i] = results.getMetaData().getColumnLabel(i+1);
	        }
	        
	        model.setColumnIdentifiers(obj);
	        
			while (results.next()) {
		        
	            Object[] row = new Object[columns];
	            for (int i=0; i<model.getColumnCount(); i++)
	            {  
	                row[i] = results.getObject(i+1);
	            }
	            
	            model.addRow(row);

			}
			results.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		if (model.getRowCount() < 2) {
			System.out.println("empty set");
			return null;
		}
		
		return model;
		
	}
	
	/**
	 * 
	 * @param SSID
	 * @return DefaultTableModel model to set to JTable
	 */
	public DefaultTableModel selectSomeSecurityWifiInfoViewData(String SSID){
		
		Statement stmt = null;
		ResultSet results = null;
		
		DefaultTableModel model = new DefaultTableModel();
		
		System.out.println("Creating statement...");
		try {
			stmt = conn.createStatement();
		} catch (SQLException e) {
			e.printStackTrace();
		}
			
		
		
		//select WiFiInfoView.SSID, WiFiInfoView.Security, WiFiInfoView.Cipher from mydb.WiFiInfoView where (WiFiInfoView.Security = "WPA-PSK" or WiFiInfoView.Security = "WEP");

		try {
			if (SSID == null ){
				results = stmt.executeQuery("SELECT `WiFiInfoView`.`SSID`, `WiFiInfoView`.`Security`,"
						+ "`WiFiInfoView`.`Cipher` "
						+ "FROM `mydb`.`WiFiInfoView` "
						+ "WHERE (`WiFiInfoView`.`Security` = \"WPA-PSK\" or `WiFiInfoView`.`Security` = \"WEP\") ");
			} else {
				results = stmt.executeQuery("SELECT `WiFiInfoView`.`SSID`, `WiFiInfoView`.`Security`,"
						+ "`WiFiInfoView`.`Cipher` "
						+ "FROM `mydb`.`WiFiInfoView` "
						+ "WHERE (`WiFiInfoView`.`Security` = \"WPA-PSK\" or `WiFiInfoView`.`Security` = \"WEP\") AND `WiFiInfoView`.`SSID` = '" + SSID + "'");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		if (results == null) {
			return null;
		}		
		
		try {
			
	        int columns = results.getMetaData().getColumnCount();
	        System.out.println(columns);
	        
	        Object[] obj = new Object[columns];
	        
	        for (int i=0;i<columns;i++){
	        	obj[i] = results.getMetaData().getColumnLabel(i+1);
	        }
	        
	        model.setColumnIdentifiers(obj);
	        
			while (results.next()) {
		        
	            Object[] row = new Object[columns];
	            for (int i=0; i<model.getColumnCount(); i++)
	            {  
	                row[i] = results.getObject(i+1);
	            }
	            
	            model.addRow(row);

			}
			results.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		if (model.getRowCount() < 2) {
			System.out.println("empty set");
			return null;
		}
		
		return model;
		
	}
	
	/**
	 * 
	 * @param SSID
	 * @return DefaultTableModel model to set to JTable
	 */
	public DefaultTableModel selectChannelUsageWifiInfoViewData_Model(String SSID){
		
		Statement stmt = null;
		ResultSet results = null;
		
		DefaultTableModel model = new DefaultTableModel();
		
		System.out.println("Creating statement...");
		try {
			stmt = conn.createStatement();
		} catch (SQLException e) {
			e.printStackTrace();
		}
			
		//SELECT * FROM mydb.WiFiInfoView where (WiFiInfoView.Channel != 1 and WiFiInfoView.Channel != 6 and WiFiInfoView.Channel != 11);
		try {
			if (SSID == null){
				results = stmt.executeQuery("SELECT `WiFiInfoView`.`SSID`, `WiFiInfoView`.`Channel` "
						+ "FROM `mydb`.`WiFiInfoView` "
						+ "WHERE (`WiFiInfoView`.`Channel` != 1 AND `WiFiInfoView`.`Channel` != 6 AND `WiFiInfoView`.`Channel` != 11) ");
			} else {
				results = stmt.executeQuery("SELECT `WiFiInfoView`.`SSID`, `WiFiInfoView`.`Channel` "
						+ "FROM `mydb`.`WiFiInfoView` "
						+ "WHERE (`WiFiInfoView`.`Channel` != 1 AND `WiFiInfoView`.`Channel` != 6 AND `WiFiInfoView`.`Channel` != 11) "
						+ "AND `WiFiInfoView`.`SSID` = '" + SSID + "'");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		if (results == null) {
			return null;
		}	
		
		
		try {
			
	        int columns = results.getMetaData().getColumnCount();
	        System.out.println(columns);
	        
	        Object[] obj = new Object[columns];
	        
	        for (int i=0;i<columns;i++){
	        	obj[i] = results.getMetaData().getColumnLabel(i+1);
	        }
	        
	        model.setColumnIdentifiers(obj);
	        
			while (results.next()) {
		        
				Object[] row = new Object[columns];
	            for (int i=0; i<model.getColumnCount(); i++)
	            {  
	                row[i] = results.getObject(i+1);
	            }
	            
	            model.addRow(row);

			}
			results.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		if (model.getRowCount() < 2) {
			System.out.println("empty set");
			return null;
		}
		
		return model;
		
	}
	
	
	/**
	 * 
	 * @param SSID
	 * @return channel usage in string format
	 */
	public String selectChannelUsageWifiInfoViewData_Opt(String SSID){
		
		Statement stmt = null;
		ResultSet results = null;
		
		StringBuilder text = new StringBuilder("");
		
		List<IntPair> pairs = new ArrayList<IntPair>();
		
		System.out.println("Creating statement...");
		try {
			stmt = conn.createStatement();
		} catch (SQLException e) {
			e.printStackTrace();
		}
			
		//SELECT * FROM mydb.WiFiInfoView where (WiFiInfoView.Channel != 1 and WiFiInfoView.Channel != 6 and WiFiInfoView.Channel != 11);
		try {
			if (SSID ==  null){
				results = stmt.executeQuery("SELECT `WiFiInfoView`.`SSID` "
						+ "FROM `mydb`.`WiFiInfoView` "
						+ "WHERE (`WiFiInfoView`.`Channel` != 1 AND `WiFiInfoView`.`Channel` != 6 AND `WiFiInfoView`.`Channel` != 11)");
			} else {
				results = stmt.executeQuery("SELECT `WiFiInfoView`.`SSID` "
						+ "FROM `mydb`.`WiFiInfoView` "
						+ "WHERE (`WiFiInfoView`.`Channel` != 1 AND `WiFiInfoView`.`Channel` != 6 AND `WiFiInfoView`.`Channel` != 11) "
						+ "AND `WiFiInfoView`.`SSID` = '" + SSID + "'");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		if (results == null) {
			return null;
		}	
		
		pairs = this.selectChannelUsageWifiInfoViewData_Channel_Counting();
		
		try {
			while (results.next()) {
				
				int min = pairs.get(0).getCounter();
				int position = 0;
				
				for(int i=1;i<pairs.size();i++){
					if(min > pairs.get(i).getCounter()){
						min = pairs.get(i).getCounter();
						position = i;
					}
				}
				
				pairs.get(position).setCounter(pairs.get(position).getCounter() + 1);
				
				text.append("Switching Network : \"");
				text.append(results.getString("SSID"));
				text.append("\"  to channel : ");
				
				//calculating best channel to switch to
				text.append(Integer.toString(pairs.get(position).getChannel()));
				text.append("\n\n");

			}
			results.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return text.toString();
		
	}
	
	/**
	 * 
	 * @return List<IntPair> of number of channels and for each channel its occurence number
	 */
	private List<IntPair> selectChannelUsageWifiInfoViewData_Channel_Counting(){
		
		Statement stmt = null;
		ResultSet results = null;
		
		List<IntPair> pairs = new ArrayList<IntPair>();
		
		System.out.println("Creating statement...");
		try {
			stmt = conn.createStatement();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		//SELECT WiFiInfoView.Channel,count(*) AS Counter FROM mydb.WiFiInfoView group by WiFiInfoView.Channel order by count(*) desc;
		//where (WiFiInfoView.Channel = 1 or WiFiInfoView.Channel = 6 or WiFiInfoView.Channel = 11)
		try {
			results = stmt.executeQuery("SELECT `WiFiInfoView`.`Channel`, count(*) AS Counter "
							+ "FROM `mydb`.`WiFiInfoView` "
							+ "WHERE (`WiFiInfoView`.`Channel` = 1 or `WiFiInfoView`.`Channel` = 6 or `WiFiInfoView`.`Channel` = 11) "
							+ "GROUP BY `WiFiInfoView`.`Channel` "
							+ "ORDER BY count(*) ASC; ");
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		if (results == null) {
			//return model;
			return null;
		}		
		
		try {
			while (results.next()) {
		        
		        int Channel = Integer.parseInt(results.getString("Channel"));
		        int Counter = Integer.parseInt(results.getString("Counter"));
		        
		        IntPair p = new IntPair(Channel,Counter);
		        
		        pairs.add(p);

			}
			results.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return pairs;
		
	}
	
}
