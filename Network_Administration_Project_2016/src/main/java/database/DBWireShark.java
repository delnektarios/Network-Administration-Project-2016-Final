package database;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.table.DefaultTableModel;

import GUI.MainGUI;

/**
 * Network Management Project
 * 2016 Athens, Greece
 * National and Kapodistrian University of Athens (NKUA)
 *
 * created by:
 * @author Deligiannakis Nektarios		sdi1200030[at]di.uoa.gr
 * @author Milarokostas Christos		sdi1200110[at]di.uoa.gr
 *
 */
public class DBWireShark extends Database {

	/**
	 * 
	 * @param cred
	 */
	public DBWireShark(DBCredentials_and_ConnectionPool cred) {
		super(cred);
	}
	
	/**
	 * 
	 * @param No
	 * @param Time
	 * @param Source
	 * @param Destination
	 * @param Protocol
	 * @param Length
	 * @param Info
	 * @return
	 */
	public int insert_DBWireShark_stats (String No,String Time,String Source,String Destination,String Protocol,String Length,String Info){
		
		Statement stmt = null;
		System.out.println("Creating statement...");
		try {
			stmt = conn.createStatement();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		

		try {	
			
			//http://stackoverflow.com/questions/5005388/cannot-add-or-update-a-child-row-a-foreign-key-constraint-fails
			stmt.executeQuery("SET FOREIGN_KEY_CHECKS=0");
			
			stmt.executeUpdate("INSERT INTO WireShark(No,Time,Source,Destination,Protocol,Length,Info)"
					+ " VALUES('" 
					+ No
					+ "','"
					+ Time
					+ "','"
					+ Source
					+ "','"
					+ Destination
					+ "','"
					+ Protocol
					+ "','"
					+ Length
					+ "','"
					+ Info + "');");	
			
			stmt.executeQuery("SET FOREIGN_KEY_CHECKS=1");
			
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return 0;
	}
	
	/**
	 * 
	 * @return DefaultTableModel model to set to JTable
	 */
	public DefaultTableModel selectWireSharkData(){
		
		Statement stmt = null;
		ResultSet results = null;
		
		DefaultTableModel model = new DefaultTableModel();
		
		System.out.println("Creating statement...");
		try {
			stmt = conn.createStatement();
		} catch (SQLException e) {
			e.printStackTrace();
		}
				
		try {
			results = stmt.executeQuery("SELECT `WireShark`.`No`, `WireShark`.`Time`,"
							+ "`WireShark`.`Source`,`WireShark`.`Destination`, "
							+ "`WireShark`.`Protocol`,`WireShark`.`Length`,"
							+ "`WireShark`.`Info` "
							+ "FROM `mydb`.`WireShark` ");
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		if (results == null) {
			return null;
		}		
		
		try {
			
	        int columns = results.getMetaData().getColumnCount();
	        System.out.println(columns);
	        
	        Object[] obj = new Object[columns];
	        
	        for (int i=0;i<columns;i++){
	        	obj[i] = results.getMetaData().getColumnLabel(i+1);
	        }
	        
	        model.setColumnIdentifiers(obj);
	        
			while (results.next()) {
		        
	            Object[] row = new Object[columns];
	            for (int i=0; i<model.getColumnCount(); i++)
	            {  
	                row[i] = results.getObject(i+1);
	            }
	            
	            model.addRow(row);

			}
			results.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		if (model.getRowCount() < 2) {
			System.out.println("empty set");
			return null;
		}
		
		return model;
		
	}

}
