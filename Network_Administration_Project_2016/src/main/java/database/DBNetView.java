package database;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.table.DefaultTableModel;

/**
 * Network Management Project
 * 2016 Athens, Greece
 * National and Kapodistrian University of Athens (NKUA)
 *
 * created by:
 * @author Deligiannakis Nektarios		sdi1200030[at]di.uoa.gr
 * @author Milarokostas Christos		sdi1200110[at]di.uoa.gr
 *
 */
public class DBNetView extends Database {

	/**
	 * 
	 * @param cred
	 */
	public DBNetView(DBCredentials_and_ConnectionPool cred) {
		super(cred);
	}
	
	/**
	 * 
	 * @param SSID
	 * @param Last_Signal
	 * @param Average_Signal
	 * @param Detection_Counter
	 * @param Security_Enabled
	 * @param Connectable
	 * @param Authentication
	 * @param Cipher
	 * @param PHY_Address
	 * @param First_Detected
	 * @param Last_Detected
	 * @param MAC_Address
	 * @param RSSI
	 * @param Channel_Frequency
	 * @param Channel_Number
	 * @param Company
	 * @param Maximum_Speed
	 * @return 0 on success
	 */
	public int insert_DBNetView_stats (String SSID,String Last_Signal,String Average_Signal,
			String Detection_Counter,String Security_Enabled,String Connectable,
			String Authentication,String Cipher,String PHY_Address,String First_Detected,
			String Last_Detected,String MAC_Address,String RSSI,String Channel_Frequency,
			String Channel_Number,String Company,String Maximum_Speed){
		
		Statement stmt = null;
		System.out.println("Creating statement...");
		try {
			stmt = conn.createStatement();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		

		try {		
			//http://stackoverflow.com/questions/5005388/cannot-add-or-update-a-child-row-a-foreign-key-constraint-fails
			stmt.executeQuery("SET FOREIGN_KEY_CHECKS=0");
			
			
			stmt.executeUpdate("INSERT INTO NetView(SSID,Last_Signal,Average_Signal,Detection_Counter,Security_Enabled,"
					+ "Connectable,Authentication,Cipher,PHY_Type,First_Detected,Last_Detected,MAC_Address,RSSI,"
					+ "Channel_Frequency,Channel_Number,Company,Maximum_Speed) VALUES('"
					+ SSID
					+ "','"
					+ Last_Signal
					+ "','"
					+ Average_Signal
					+ "','"
					+ Detection_Counter
					+ "','"
					+ Security_Enabled
					+ "','"
					+ Connectable 
					+ "','"
					+ Authentication 
					+ "','"
					+ Cipher 
					+ "','"
					+ PHY_Address 
					+ "','"
					+ First_Detected 
					+ "','"
					+ Last_Detected 
					+ "','"
					+ MAC_Address 
					+ "','"
					+ RSSI 
					+ "','"
					+ Channel_Frequency 
					+ "','"
					+ Channel_Number 
					+ "','"
					+ Company 
					+ "','"
					+ Maximum_Speed + "')");	
			
			stmt.executeQuery("SET FOREIGN_KEY_CHECKS=1");
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return 0;
	}
	
	/**
	 * 
	 * @param SSID
	 * @return DefaultTableModel model to set to JTable
	 */
	public DefaultTableModel selectNetViewData(String SSID){
		
		Statement stmt = null;
		ResultSet results = null;
		
		DefaultTableModel model = new DefaultTableModel();
		
		System.out.println("Creating statement...");
		try {
			stmt = conn.createStatement();
		} catch (SQLException e) {
			e.printStackTrace();
		}
				
		try {
			if ( SSID == null) {
				results = stmt.executeQuery("SELECT `NetView`.`SSID`, `NetView`.`Last_Signal`,"
						+ "`NetView`.`Average_Signal`,`NetView`.`Detection_Counter`, "
						+ "`NetView`.`Security_Enabled`,`NetView`.`Channel_Frequency`,"
						+ "`NetView`.`Connectable`,`NetView`.`Authentication`, "
						+ "`NetView`.`Cipher`,`NetView`.`PHY_Type`,`NetView`.`First_Detected`,"
						+ "`NetView`.`Last_Detected`, "
						+ "`NetView`.`MAC_Address`,`NetView`.`RSSI`,"
						+ "`NetView`.`Channel_Frequency`,`NetView`.`Channel_Number`, "
						+ "`NetView`.`Company`,`NetView`.`Maximum_Speed`"
						+ "FROM `mydb`.`NetView` ");
			} else {
				results = stmt.executeQuery("SELECT `NetView`.`SSID`, `NetView`.`Last_Signal`,"
						+ "`NetView`.`Average_Signal`,`NetView`.`Detection_Counter`, "
						+ "`NetView`.`Security_Enabled`,`NetView`.`Channel_Frequency`,"
						+ "`NetView`.`Connectable`,`NetView`.`Authentication`, "
						+ "`NetView`.`Cipher`,`NetView`.`PHY_Type`,`NetView`.`First_Detected`,"
						+ "`NetView`.`Last_Detected`, "
						+ "`NetView`.`MAC_Address`,`NetView`.`RSSI`,"
						+ "`NetView`.`Channel_Frequency`,`NetView`.`Channel_Number`, "
						+ "`NetView`.`Company`,`NetView`.`Maximum_Speed`"
						+ "FROM `mydb`.`NetView` "
						+ "`NetView`.`SSID` = '" + SSID + "'");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		if (results == null) {
			return null;
		}		
		
		try {
			
	        int columns = results.getMetaData().getColumnCount();
	        System.out.println(columns);
	        
	        Object[] obj = new Object[columns];
	        
	        for (int i=0;i<columns;i++){
	        	obj[i] = results.getMetaData().getColumnLabel(i+1);
	        }
	        
	        model.setColumnIdentifiers(obj);
	        
			while (results.next()) {
		        
	            Object[] row = new Object[columns];
	            for (int i=0; i<model.getColumnCount(); i++)
	            {  
	                row[i] = results.getObject(i+1);
	            }
	            
	            model.addRow(row);
	            
			}
			results.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		if (model.getRowCount() < 2) {
			System.out.println("empty set");
			return null;
		}

		return model;
		
	}
	
	/**
	 * 
	 * @param SSID
	 * @return DefaultTableModel model to set to JTable
	 */
	public DefaultTableModel selectSecurityNoneNetViewData(String SSID){
		
		Statement stmt = null;
		ResultSet results = null;
		
		DefaultTableModel model = new DefaultTableModel();
		
		System.out.println("Creating statement...");
		try {
			stmt = conn.createStatement();
		} catch (SQLException e) {
			e.printStackTrace();
		}
			
		//select NetView.SSID, NetView.Security_Enabled, NetView.Cipher from mydb.NetView where (NetView.Security_Enabled = "No");
		
		try {
			if(SSID == null){
				results = stmt.executeQuery("SELECT `NetView`.`SSID`, `NetView`.`Security_Enabled`,"
						+ "`NetView`.`Cipher`"
						+ "FROM `mydb`.`NetView`"
						+ "WHERE (`NetView`.`Security_Enabled` = \"No\") ");
			} else {
				results = stmt.executeQuery("SELECT `NetView`.`SSID`, `NetView`.`Security_Enabled`,"
						+ "`NetView`.`Cipher`"
						+ "FROM `mydb`.`NetView`"
						+ "WHERE (`NetView`.`Security_Enabled` = \"No\") AND `NetView`.`SSID` = '" + SSID + "'");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		if (results == null) {
			return null;
		}		
		
		try {
			
	        int columns = results.getMetaData().getColumnCount();
	        System.out.println(columns);
	        
	        Object[] obj = new Object[columns];
	        
	        for (int i=0;i<columns;i++){
	        	obj[i] = results.getMetaData().getColumnLabel(i+1);
	        }
	        
	        model.setColumnIdentifiers(obj);
	        
			while (results.next()) {
		        
	            Object[] row = new Object[columns];
	            for (int i=0; i<model.getColumnCount(); i++)
	            {  
	                row[i] = results.getObject(i+1);
	            }
	            
	            model.addRow(row);
	            
			}
			results.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		if (model.getRowCount() < 2) {
			System.out.println("empty set");
			return null;
		}
		
		return model;
		
	}
	
	/**
	 * 
	 * @param SSID
	 * @return DefaultTableModel model to set to JTable
	 */
	public DefaultTableModel selectSomeSecurityNetViewData(String SSID){
		
		Statement stmt = null;
		ResultSet results = null;
		
		DefaultTableModel model = new DefaultTableModel();
		
		System.out.println("Creating statement...");
		try {
			stmt = conn.createStatement();
		} catch (SQLException e) {
			e.printStackTrace();
		}
			
		//select NetView.SSID, NetView.Security_Enabled, NetView.Authentication, NetView.Cipher from mydb.NetView where (NetView.Authentication = "802.11 Open");

		
		try {
			if ( SSID == null){
				results = stmt.executeQuery("SELECT `NetView`.`SSID`, `NetView`.`Security_Enabled`,"
						+ "`NetView`.`Authentication` ,`NetView`.`Cipher`"
						+ "FROM `mydb`.`NetView`"
						+ "WHERE (`NetView`.`Authentication` = \"802.11 Open\") ");
			} else {
				results = stmt.executeQuery("SELECT `NetView`.`SSID`, `NetView`.`Security_Enabled`,"
						+ "`NetView`.`Authentication` ,`NetView`.`Cipher`"
						+ "FROM `mydb`.`NetView`"
						+ "WHERE (`NetView`.`Authentication` = \"802.11 Open\") AND `NetView`.`SSID` = '" + SSID + "'");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		if (results == null) {
			return null;
		}		
		
		try {
			
	        int columns = results.getMetaData().getColumnCount();
	        System.out.println(columns);
	        
	        Object[] obj = new Object[columns];
	        
	        for (int i=0;i<columns;i++){
	        	obj[i] = results.getMetaData().getColumnLabel(i+1);
	        }
	        
	        model.setColumnIdentifiers(obj);
	        
			while (results.next()) {
		        
	            Object[] row = new Object[columns];
	            for (int i=0; i<model.getColumnCount(); i++)
	            {  
	                row[i] = results.getObject(i+1);
	            }
	            
	            model.addRow(row);

			}
			results.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		if (model.getRowCount() < 2) {
			System.out.println("empty set");
			return null;
		}
		
		return model;
		
	}
	
	/**
	 * 
	 * @param SSID
	 * @return DefaultTableModel model to set to JTable
	 */
	public DefaultTableModel selectNetworkNetViewData(String SSID){
		
		Statement stmt = null;
		ResultSet results = null;
		
		DefaultTableModel model = new DefaultTableModel();
		
		System.out.println("Creating statement...");
		try {
			stmt = conn.createStatement();
		} catch (SQLException e) {
			e.printStackTrace();
		}
			

		//select NetView.SSID, NetView.PHY_Type, NetView.Maximum_Speed, NetView.MAC_Address from mydb.NetView where (PHY_Type != "802.11n");
		try {
			if (SSID == null){
				results = stmt.executeQuery("SELECT `NetView`.`SSID`, `NetView`.`PHY_Type`, "
						+ "`NetView`.`Maximum_Speed`, `NetView`.`MAC_Address` "
						+ "FROM `mydb`.`NetView`"
						+ "WHERE (`NetView`.`PHY_Type` != \"802.11n\") ");
			} else {
				results = stmt.executeQuery("SELECT `NetView`.`SSID`, `NetView`.`PHY_Type`, "
						+ "`NetView`.`Maximum_Speed`, `NetView`.`MAC_Address` "
						+ "FROM `mydb`.`NetView`"
						+ "WHERE (`NetView`.`PHY_Type` != \"802.11n\") AND `NetView`.`SSID` = '" + SSID + "'");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		if (results == null) {
			return null;
		}		
		
		try {
			
			
	        int columns = results.getMetaData().getColumnCount();
	        System.out.println(columns);
	        
	        Object[] obj = new Object[columns];
	        
	        for (int i=0;i<columns;i++){
	        	obj[i] = results.getMetaData().getColumnLabel(i+1);
	        }
	        
	        model.setColumnIdentifiers(obj);
	        
			while (results.next()) {
		        
	            Object[] row = new Object[columns];
	            for (int i=0; i<model.getColumnCount(); i++)
	            {  
	                row[i] = results.getObject(i+1);
	            }
	            
	            model.addRow(row);

			}
			results.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		if (model.getRowCount() < 2) {
			System.out.println("empty set");
			return null;
		}
		
		return model;
		
	}
	

}
