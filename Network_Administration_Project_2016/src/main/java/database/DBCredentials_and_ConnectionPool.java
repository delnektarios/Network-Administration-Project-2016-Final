package database;

import com.jolbox.bonecp.BoneCP;

/**
 * Network Management Project
 * 2016 Athens, Greece
 * National and Kapodistrian University of Athens (NKUA)
 *
 * created by:
 * @author Deligiannakis Nektarios		sdi1200030[at]di.uoa.gr
 * @author Milarokostas Christos		sdi1200110[at]di.uoa.gr
 *
 */

public class DBCredentials_and_ConnectionPool {
	
	String username;
	String password;
	String url;
	BoneCP connectionPool;

	/**
	 * 
	 * @param url
	 * @param username
	 * @param password
	 * @param connectionPool
	 */
	public DBCredentials_and_ConnectionPool( String url, String username, String password, BoneCP connectionPool) {
		this.username = username;
		this.password = password;
		this.url = url;
		this.connectionPool = connectionPool;
	}
}
