package database;

import java.sql.*;

/**
 * Network Management Project
 * 2016 Athens, Greece
 * National and Kapodistrian University of Athens (NKUA)
 *
 * created by:
 * @author Deligiannakis Nektarios		sdi1200030[at]di.uoa.gr
 * @author Milarokostas Christos		sdi1200110[at]di.uoa.gr
 *
 */
public class DBLogin extends Database {

	/**
	 * 
	 * @param cred
	 */
	public DBLogin(DBCredentials_and_ConnectionPool cred) {
		super(cred);
	}
	
	/**
	 * 
	 * @param username
	 * @param password
	 * @return boolean with validation of login / true for successful login
	 */
	public boolean validateLogin(String username, String password) {
		String p = VerifyLogin2DB(username);
		if (p == null) {
			return false;
		} else if (p.equals(password)) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * 
	 * @param username
	 * @return passsword for user with username
	 */
	public String VerifyLogin2DB(String username){
		
		Statement stmt = null;
		ResultSet results=null;
		String password=null;
		
		System.out.println("Creating statement...");
	    try {
			stmt = conn.createStatement();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    try {
			results = stmt.executeQuery("SELECT Administrator ,Password  FROM Administrators WHERE Administrator='"+username+"'");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    
	    if(results==null){ 
	    	return null;
	    }
	    
	    try {
			if(results.next()){
				password = results.getString("password");
			}
			results.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    
	   return password;
	}
	
	
}
