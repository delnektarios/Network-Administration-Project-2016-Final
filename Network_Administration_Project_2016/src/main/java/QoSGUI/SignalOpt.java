package QoSGUI;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import Actions.ExitButtonAction_JustDispose;
import GUI.MainGUI;
import database.DBXirrus;

/**
 * Network Management Project
 * 2016 Athens, Greece
 * National and Kapodistrian University of Athens (NKUA)
 *
 * created by:
 * @author Deligiannakis Nektarios		sdi1200030[at]di.uoa.gr
 * @author Milarokostas Christos		sdi1200110[at]di.uoa.gr
 *
 */

public class SignalOpt {

	private JFrame frame;
	private JTable table;


	/**
	 * Create the application.
	 */
	public SignalOpt() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 708, 391);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		frame.setTitle("Signal Usage");
		frame.setLocationRelativeTo(null);
		frame.setResizable(false);
		
		//HERE TO SET DATA FROM DATABASE!!!
		
		DBXirrus xirrus = new DBXirrus(project.Main.getDb_credentials());
		DefaultTableModel model = xirrus.selectXirrusSignalData(MainGUI.getSSID_only());
		xirrus.close_database_connection();
		
		if(model == null){
            JOptionPane.showMessageDialog(null, "Something  went wrong!\n"
            		+ "Posibly empty database!\nOr no optimization needed!", "Completed", JOptionPane.INFORMATION_MESSAGE);
            frame.dispose();
		}
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(12, 12, 682, 275);
		frame.getContentPane().add(scrollPane);
		
		table = new JTable();
		scrollPane.setViewportView(table);
		
		table.setCellSelectionEnabled(true);
		//table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF)
		
		table.setModel(model);
		scrollPane.setViewportView(table);
				
		JButton btnClose = new JButton("Close");
		btnClose.addActionListener(new ExitButtonAction_JustDispose(frame));
		btnClose.setBounds(557, 310, 117, 25);
		frame.getContentPane().add(btnClose);
	}

	public JFrame getFrame() {
		return frame;
	}

	public void setFrame(JFrame frame) {
		this.frame = frame;
	}
}
