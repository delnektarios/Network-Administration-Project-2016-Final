package PropertyFileReaders;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

/**
 * Network Management Project
 * 2016 Athens, Greece
 * National and Kapodistrian University of Athens (NKUA)
 *
 * created by:
 * @author Deligiannakis Nektarios		sdi1200030[at]di.uoa.gr
 * @author Milarokostas Christos		sdi1200110[at]di.uoa.gr
 *
 */

public class ReadCommonModels {

	private Properties prop = null;
	private FileReader reader ;
	
	/**
	 * read all the common router models to show in User panel
	 * @param path path of property file
	 */
	public ReadCommonModels(String path){
		File properties_file = new File(path);
		this.prop = new Properties();
		
		try {
			this.reader = new FileReader(properties_file) ;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
				
		try {
			prop.load(reader);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	/**
	 * Get all available models
	 * @return model names and brands in an array of strings
	 */
	public String[] getAvailableMODELS(){
		String models = prop.getProperty("MODELS");
		//System.out.println(models);
		String[] args = models.split(",");
		return args;
	}
	
	/**
	 * Getting all available IPs to connect to routers
	 * @param model for the chosen router model
	 * @return the IPs addresses in string format array
	 */
	public String[] getAvailableIPs(String model){
		String ips = prop.getProperty(model);
		System.out.println(model + "in redaesr"+ " " + ips);
		String[] args = ips.split(",");
		return args;
	}
	
}
