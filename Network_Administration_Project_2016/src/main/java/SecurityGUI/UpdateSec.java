package SecurityGUI;


import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import Actions.ExitButtonAction_JustDispose;
import GUI.MainGUI;
import database.DBNetView;

/**
 * Network Management Project
 * 2016 Athens, Greece
 * National and Kapodistrian University of Athens (NKUA)
 *
 * created by:
 * @author Deligiannakis Nektarios		sdi1200030[at]di.uoa.gr
 * @author Milarokostas Christos		sdi1200110[at]di.uoa.gr
 *
 */

public class UpdateSec {

	private JFrame frame;
	private JTable table;
	private JTable table1;
	private JScrollPane scrollPane;
	private JScrollPane scrollPane1;

	/**
	 * Create the application.
	 */
	public UpdateSec() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		setFrame(new JFrame());
		getFrame().setBounds(100, 100, 842, 472);
		getFrame().setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		frame.setTitle("Updating Security");
		frame.setLocationRelativeTo(null);
		
		//HERE TO SET DATA FROM DATABASE!!!
		
		DBNetView netview = new DBNetView(project.Main.getDb_credentials());
		DefaultTableModel model = netview.selectSomeSecurityNetViewData(MainGUI.getSSID_only());
		netview.close_database_connection();
		
		if(model == null){
            JOptionPane.showMessageDialog(null, "Something  went wrong!\n"
            		+ "Posibly empty database!\nOr no optimization needed!", "Completed", JOptionPane.INFORMATION_MESSAGE);
            frame.dispose();
		}
		
		scrollPane = new JScrollPane();
		scrollPane.setBounds(12, 12, 808, 156);
		frame.getContentPane().add(scrollPane);
		
		table = new JTable();
		scrollPane.setViewportView(table);
		
		//table = new JTable();
		table.setCellSelectionEnabled(true);
		//table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		
		table.setModel(model);
		scrollPane.setViewportView(table);
		
		scrollPane1 = new JScrollPane();
		scrollPane1.setBounds(12, 189, 808, 156);
		frame.getContentPane().add(scrollPane1);
		
		table1 = new JTable();
		table1.setCellSelectionEnabled(true);
		//table1.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		
		table1.setModel(model);
		scrollPane1.setViewportView(table1);
		
		
		JButton btnClose = new JButton("Close");
		btnClose.setBounds(703, 392, 117, 25);
		btnClose.addActionListener(new ExitButtonAction_JustDispose(frame));
		frame.getContentPane().add(btnClose);

		
	}
	
	public JFrame getFrame() {
		return frame;
	}

	public void setFrame(JFrame frame) {
		this.frame = frame;
	}
	
}
