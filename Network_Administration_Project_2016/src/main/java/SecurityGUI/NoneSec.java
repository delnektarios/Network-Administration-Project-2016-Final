package SecurityGUI;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import Actions.ExitButtonAction_JustDispose;
import GUI.MainGUI;
import database.DBWiFiInfoView;

/**
 * 
 * @author netarios
 * @author netarios
 *
 */
public class NoneSec {

	private JFrame frame;
	private JTable table;
	private JScrollPane scrollPane;

	/**
	 * Create the application.
	 */
	public NoneSec() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		setFrame(new JFrame());
		getFrame().setBounds(100, 100, 842, 472);
		getFrame().setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.setTitle("Updating Non Existing Security");
		frame.setLocationRelativeTo(null);
		//HERE TO SET DATA FROM DATABASE!!!
		
		DBWiFiInfoView wifiinfoview = new DBWiFiInfoView(project.Main.getDb_credentials());
		DefaultTableModel model = wifiinfoview.selectSecurityNoneWifiInfoViewData(MainGUI.getSSID_only());
		wifiinfoview.close_database_connection();
		
		if(model == null){
            JOptionPane.showMessageDialog(null, "Something  went wrong!\n"
            		+ "Posibly empty database!\nOr no optimization needed!", "Completed", JOptionPane.INFORMATION_MESSAGE);
            frame.dispose();
		}
		
		scrollPane = new JScrollPane();
		scrollPane.setBounds(12, 42, 816, 304);
		frame.getContentPane().add(scrollPane);
		
		table = new JTable();
		
		table.setModel(model);
		scrollPane.setViewportView(table);
		
		JButton btnClose = new JButton("Close");
		btnClose.setBounds(682, 387, 134, 32);
		btnClose.addActionListener(new ExitButtonAction_JustDispose(frame));
		frame.getContentPane().setLayout(null);
		getFrame().getContentPane().add(btnClose);
		
	}
	
	public JFrame getFrame() {
		return frame;
	}

	public void setFrame(JFrame frame) {
		this.frame = frame;
	}
}
