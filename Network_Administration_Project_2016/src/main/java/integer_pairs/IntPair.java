package integer_pairs;

/**
 * Network Management Project
 * 2016 Athens, Greece
 * National and Kapodistrian University of Athens (NKUA)
 *
 * created by:
 * @author Deligiannakis Nektarios		sdi1200030[at]di.uoa.gr
 * @author Milarokostas Christos		sdi1200110[at]di.uoa.gr
 *
 */

public class IntPair {
	
	private int Channel = 0;
	private int Counter = 0;
	
	/**
	 * Creates a list with integer pairs
	 * @param x
	 * @param y
	 */
	public IntPair(int x, int y) {this.Channel=x;this.Counter=y;}
	  
	public void setCounter(int counter) {
		Counter = counter;
	}

	public int getChannel() {
		return Channel;
	}
	public int getCounter() {
		return Counter;
	}
}
