package FileChooser;

import java.io.File;

import javax.swing.filechooser.FileFilter;

/**
 * Network Management Project
 * 2016 Athens, Greece
 * National and Kapodistrian University of Athens (NKUA)
 *
 * created by:
 * @author Deligiannakis Nektarios		sdi1200030[at]di.uoa.gr
 * @author Milarokostas Christos		sdi1200110[at]di.uoa.gr
 *
 */

public class FileTypeChooserFilter extends FileFilter {
	
	private final String fileExtension;
	private final String Description;
	
	/**
	 * 
	 * @param fileExtension
	 * @param Description
	 */
	public FileTypeChooserFilter(String fileExtension,String Description){
		this.fileExtension = fileExtension;
		this.Description = Description;
	}

	@Override
	public boolean accept(File file) {
		if(file.isDirectory()){
			return true;
		}
		return file.getName().endsWith(fileExtension);
	}

	@Override
	public String getDescription() {
		return Description + String.format(" (*%s)", fileExtension);
	}
	
	

}
