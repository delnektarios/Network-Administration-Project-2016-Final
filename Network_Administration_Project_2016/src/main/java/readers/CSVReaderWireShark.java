package readers;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;

import javax.swing.JProgressBar;
import javax.swing.JTextArea;

import GUI.MainGUI;
import project.Main;
import database.DBWireShark;

/**
 * Network Management Project
 * 2016 Athens, Greece
 * National and Kapodistrian University of Athens (NKUA)
 *
 * created by:
 * @author Deligiannakis Nektarios		sdi1200030[at]di.uoa.gr
 * @author Milarokostas Christos		sdi1200110[at]di.uoa.gr
 *
 */

public class CSVReaderWireShark {
	
	private DBWireShark wireshark = null;
	private JProgressBar progressBar;
	private JTextArea textArea;
	
	private String csvFile;
	private long size = 0;
	private boolean stop = false;
	
	private int SYN_counter= 0;
	
	
	/**
	 * 
	 * @param absolutePath
	 * @param progressBar
	 * @param textArea
	 */
	public CSVReaderWireShark(String absolutePath, JProgressBar progressBar, JTextArea textArea) {
		this.csvFile = absolutePath;
		File file = new File(this.csvFile);
		size = file.length();
		file.setReadable(true);
		this.progressBar = progressBar;
		this.textArea = textArea;
	}

	/**
	 * 
	 */
	public void upload_CSVReaderWireShark_info() {

        //String csvFile = "./mikel_day3(shark).csv";
        BufferedReader bufferedreader = null;
        String line = "";
        String splitted = ",";
        String remove = "\"";

        try {

        	bufferedreader = new BufferedReader(new InputStreamReader(new FileInputStream(csvFile), Charset.forName("UTF-8")));
        	line = bufferedreader.readLine();
        	
        	double lengthPerPercent = 100.0 / size;
        	long readLength = 0;
        	String potential_DOS_ATTACK_target = "";
        	
            while ((line = bufferedreader.readLine()) != null && stop!=true) {
        	
            	//System.out.println(size);
            	//System.out.println(lengthPerPercent + "/////////" + line.length());
            	
                // use comma as separator
                String[] args1 = line.split(splitted);
                     
                readLength += line.length();
                System.out.println((int) Math.round(lengthPerPercent * readLength));
                
                int percentage_done = (int) Math.round(lengthPerPercent * readLength);
                
                progressBar.setValue(percentage_done);
                
                String per_done = Integer.toString(percentage_done);
                textArea.setText(per_done+"% Done...");
                
                if(args1.length < 2) break;
                
                //Different destination!!
                if(args1[3].replace(remove, "").equals(potential_DOS_ATTACK_target) == false){
                	
                	potential_DOS_ATTACK_target = args1[3].replace(remove, "");
                	SYN_counter = 0;
                	
                } else { // SAME DESTINATION!!!!!!!
                	if (args1[6].replace(remove, "").contains("[SYN]")){
                    	SYN_counter++;
                    }
                }
                
                
                wireshark = new DBWireShark(Main.getDb_credentials());
                wireshark.insert_DBWireShark_stats(args1[0].replace(remove, ""),args1[1].replace(remove, ""),
                		args1[2].replace(remove, ""),args1[3].replace(remove, ""),args1[4].replace(remove, ""),
                		args1[5].replace(remove, ""),args1[6].replace(remove, ""));
                wireshark.close_database_connection();

            }
            
            progressBar.setValue(100);
            
            MainGUI.set_SYN_counter(SYN_counter);
            MainGUI.set_Potential_DOS_ATTACK_target(potential_DOS_ATTACK_target);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (bufferedreader != null) {
                try {
                	bufferedreader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

    }
	
	public void stop(){
		this.stop = true;
	}

}
