package readers;


import database.DBIPScanner;
import project.Main;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;

import javax.swing.JProgressBar;
import javax.swing.JTextArea;

/**
 * Network Management Project
 * 2016 Athens, Greece
 * National and Kapodistrian University of Athens (NKUA)
 *
 * created by:
 * @author Deligiannakis Nektarios		sdi1200030[at]di.uoa.gr
 * @author Milarokostas Christos		sdi1200110[at]di.uoa.gr
 *
 */

public class CSVReaderIPScanner {
	
	private static DBIPScanner dbipscanner;
	private String csvFile;
	private long size = 0;
	private JProgressBar progressBar;
	private JTextArea textArea;

	/**
	 * 
	 * @param absolutePath
	 * @param progressBar
	 * @param textArea
	 */
	public CSVReaderIPScanner(String absolutePath, JProgressBar progressBar, JTextArea textArea) {
		this.csvFile = absolutePath;
		File file = new File(this.csvFile);
		size = file.length();
		file.setReadable(true);
		this.progressBar = progressBar;
		this.textArea = textArea;
	}

	/**
	 * 
	 */
	public void upload_DBIPScanner_info() {
        
		//csvFile = "./mikel_day1(ipscanner).csv";
        BufferedReader bufferedreader = null;
        String line = "";
        String splitted = "\t";

        try {

        	bufferedreader = new BufferedReader(new InputStreamReader(new FileInputStream(csvFile), Charset.forName("UTF-16")));
        	line = bufferedreader.readLine();
        	
        	double lengthPerPercent = 100.0 / size;
        	long readLength = 0;
        	
            while ((line = bufferedreader.readLine()) != null) {
            	
                String[] args1 = line.split(splitted);
                
                if(args1.length < 2) break;
                
                readLength += line.length();
                System.out.println((int) Math.round(lengthPerPercent * readLength));
                progressBar.setValue((int) Math.round(lengthPerPercent * readLength));
                
                int percentage_done = (int) Math.round(lengthPerPercent * readLength);
                
                progressBar.setValue(percentage_done);
                
                String per_done = Integer.toString(percentage_done);
                textArea.setText(per_done+"% Done...");
                
            	dbipscanner = new DBIPScanner(Main.getDb_credentials());                
                dbipscanner.insert_DBIPScanner_stats(args1[0],args1[1],args1[2],args1[3],args1[4],args1[5],args1[6],args1[7],args1[8],args1[9],args1[10],args1[11]);
                dbipscanner.close_database_connection();

            }

            progressBar.setValue(100);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (bufferedreader != null) {
                try {
                	bufferedreader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }



	}

}
