package readers;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import javax.swing.JProgressBar;
import javax.swing.JTextArea;

import project.Main;
import database.DBWiFiInfoView;

/**
 * Network Management Project
 * 2016 Athens, Greece
 * National and Kapodistrian University of Athens (NKUA)
 *
 * created by:
 * @author Deligiannakis Nektarios		sdi1200030[at]di.uoa.gr
 * @author Milarokostas Christos		sdi1200110[at]di.uoa.gr
 *
 */

public class CSVReaderWifiInfoView {
	
	private DBWiFiInfoView dbwifiinfoview = null;
	
	private String csvFile;
	private long size = 0;
	private JProgressBar progressBar;
	private JTextArea textArea;
	
	/**
	 * 
	 * @param absolutePath
	 * @param progressBar
	 * @param textArea
	 */
    public CSVReaderWifiInfoView(String absolutePath, JProgressBar progressBar, JTextArea textArea) {
		this.csvFile = absolutePath;
		File file = new File(this.csvFile);
		size = file.length();
		file.setReadable(true);
		this.progressBar = progressBar;
		this.textArea = textArea;
	}

    /**
     * 
     */
	public void upload_CSVReaderWifiInfoView_info() {

        //String csvFile = "mikel_day2(wifiinfoview).csv";
        BufferedReader bufferedreader = null;
        String line = "";
        String splitted = ",";
        String remove = "\"";

        try {

        	bufferedreader = new BufferedReader(new InputStreamReader(new FileInputStream(csvFile), Charset.forName("UTF-8")));
        	line = bufferedreader.readLine();
        	
        	double lengthPerPercent = 100.0 / size;
        	long readLength = 0;
        	
            while ((line = bufferedreader.readLine()) != null) {

                // use comma as separator
                String[] args1 = line.split(splitted);
                
                if(args1.length < 2) break;
                
                readLength += line.length();
                System.out.println((int) Math.round(lengthPerPercent * readLength));
                progressBar.setValue((int) Math.round(lengthPerPercent * readLength));
                
                int percentage_done = (int) Math.round(lengthPerPercent * readLength);
   
                progressBar.setValue(percentage_done);
                
                String per_done = Integer.toString(percentage_done);
                textArea.setText(per_done+"% Done...");
                
                dbwifiinfoview = new DBWiFiInfoView(Main.getDb_credentials());
                
                if(args1[11].contains("\"")){
                	
                	int i = 0;
                	for(i=12;i<args1.length;i++){
                		if(args1[i].contains("\"")) break;
                	}
      
                    String Company = args1[11].replace(remove, "");
                    for(int j=12;j<i;j++){
                    	Company += args1[j].replace(remove, "");
                    }

                	dbwifiinfoview.insert_DBWiFiInfoView_stats(args1[0],args1[1],args1[2],args1[3],args1[5],
                			args1[6].replace("\"", "")+","+args1[7].replace("\"", ""),args1[8],args1[9],args1[10],
                			Company,args1[i+1],args1[i+2],args1[i+3],args1[i+4],args1[i+5],args1[i+6],args1[i+7],args1[i+8],args1[i+9]);
                    
                }else{
                	dbwifiinfoview.insert_DBWiFiInfoView_stats(args1[0],args1[1],args1[2],args1[3],args1[5],
                			args1[6].replace("\"", "")+","+args1[7].replace("\"", ""),args1[8],args1[9],args1[10],
                			args1[11],args1[12],args1[13],args1[14],args1[15],args1[16],args1[17],args1[21],args1[22],args1[23]);
                }
                
                
                dbwifiinfoview.close_database_connection();

            }
            
            progressBar.setValue(100);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (bufferedreader != null) {
                try {
                	bufferedreader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

    }

}
