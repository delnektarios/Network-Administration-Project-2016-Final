

Network Management Project
2016 Athens, Greece
National and Kapodistrian University of Athens (NKUA)

created by:
Deligiannakis Nektarios		sdi1200030[at]di.uoa.gr
Milarokostas Christos		sdi1200110[at]di.uoa.gr





Contents
========

The entire Network Management Exercise is consisted of four parts.
First of all, we have the data that where gathered through 8 days.
The data are restored in .csv files, easily accessilbe through various
programs. Secondly, we have the database that was created to organize 
our data. Thirdly, we have the 'Network Management Project' program, 
which optimize selected network(s). And finally, we have the presentation
that serves as a description of the whole Network Management Exercise.




Program Description
===================

The Network Management program is a try to fix several faults and
optimize the network(s) and user(s) experience. The program is consisted
of three parts:
*The Database
*The Quality of Service (QoS) issues
*and The Security issues
When you manage to start the program (instructions below) you will be
promted to the Home Screen (Load Data). From there you can upload several
.csv files with data of the programs (instructions below). Then you can
access all this data by opting for 'Manage Database' section and clicking
on the program's data you wish to see. The other two parts are accessible
via 'Network Administration and Optimization' section. Your first option
is to go for 'Quality of Sevice' issues. There, you can optimize the
channel a network use, search for networks with proper signal strength
(over -70dB), find the best 802.11 protocol or scan for issues regarding
the packet flow on your network. The second option is 'Security' section.
In this section you can establish a network security for non-secure networks
or upgrade the internet security of a secured, but not well-enough, network.
More info about the program you can find on the included presentation.



Pre-face
========

Before starting the program you have to have some data gathered in .csv
files in order to load them to the program. All of the programs used in
the exercise are capable of exporting data in various forms, with .csv being
the most commonly used.
*"Advanced IP Scanner" allows you to easily save your data just by selecting
'Save As' from the 'File' menu and then choose a file name and Save As 'CSV
(.csv)'.
*"WirelessNetView" follows the same procedure. You can save your data by
clicking 'File' from the menu, 'Save All Items' and then choose a file name
and Save As 'comma delimited text file (.csv)'.
*"WifiInfoView" also follows the same procedure. Save your data by clicking
'File' from the menu, 'Save All Items' and then choose a file name and
Save As 'comma delimited text file (.csv)'.
*"Xirrus Wi-Fi Inspector" has an even simpler option. Select 'Export Netwroks'
from the 'Settings' menu and then choose a file name and Save As
'comma delimited text file (.csv)'.
*"Wireshark" is a bit more complex. Once you finished and stopped your capture
you can select 'Save As' from the 'File' menu and choose a file name and Save As
'Wireshark/...-pcapng(.pcapng, .pcapng.gz, .ntar, .ntar.gz)'. Then select
'Export Packet Dissections' from the 'File' menu and select 'As CSV...'. Choose
a file name and Save As 'CSV(Comma Separated Values Summary)(.csv)'.




Getting the program started
===========================

At first, you have to open and connect to database. If you are using
Windows, Mac OS, Ubuntu Linux or Oracle Linux you can do that by
downloading the latest MySQL Installer version (version 5.7) and follow
the installer's instructions. You need to download and install both
MySQL Workbench (current version 6.3) and MySQL Server (version 5.7).
After installing MySQL Workbench and MySQL Server double click on
project.mwb to open the database via Workbench. After doing that, click
on Database->Forward Engineer (Ctrl+G) and check on parameters section
the following:
*Hostname: 'localhost'
*Port: '3306'
*Username: your_username
*Password: your_password (will be requested later)
Be careful! If you are using username other than 'root' and password
other than 'root' you have to aply the suitable changes in program's
code in class main (project):
*private static final String USER = "your_username"
*private static final String PASSWORD = "your_password"
After completing Forward Engineer you have to click on Database->Connect to Database,
keep the same parameters as before and click ok. You are good to go now!

You can launch the program via a suitable IDE. If you are using Oracle's
Eclipse (proper choise since the code is written in Java), you can click
on File->Import->Maven->Existing Maven Projects. Click on Next. Browse
program's file 'FinalProject' and finally click Finish. Then from
projectfinal -> src/main/java -> project -> main.java, right click on
main.java and choose 'Run As' -> '1 Java Application (Alt+Shift+X,J)'.
If you did everything right you should see the Home Screen of our program
(Load Data). Well Done!




Executing the program
=====================

So far, so good. Now your first step when managing to see the GUI of the
program should be to upload a .csv file. There are a couple of .csv files
included (one for each program) to properly run the program. What you
should do is select the program you wish to upload its data, click on 'Choose
file', select the proper data file and upload it. Of course, you can upload
multiple data files. Be aware of the time that each file need to be uploaded.
Larger files need more time, that's why data files of programs like
"Wireshark" request more time. In any case, your computer's specs will affect
the procedure. After uploading the .csv files you can access all the data by
clicking on 'Manage Database' section. There are 5 options-one for each
program. Select the wished program by clicking on it and you will see all the
data that were included on the file(s) you uploaded.If any table of the
database is empty a suitable message will appear warning you. Then, you can
select the 'Network Administration and Optimization' tab to administer the
networks. There are 2 main options: Quality of Service (QoS) management and
Security management.

In the QoS section you can:

*Optimize the channel usage
 --------------------------
 The decision making program finds all the networks that do not use one of the
 proper channels (1,6,11) and allocates them using the following algorithm. The
 first network that is not allocated in one of the above-mentioned channels will
 be moved to one of channels 1,6,11-the least crowded. Then the second network
 that does not use channels 1,6,11 will be again moved to the least crowded of
 these and so on. If there are 2 least crowded channels (example 1 and 11 have
 both 8 networks), we use the a tie braker and that is that we consider the channel
 that previously had the most networks as bottom.

*Signal Usage Optimizations
 --------------------------
 The decision making program finds all the networks that emit a signal below -70dB.
 The reasons that the signal strength is below -70dB are multiple. Obsolete network
 devices, great distance of the device to be signed in from the netwroks AP(s) or
 interference from nearby networks are some of them. In any case the decision making
 program presents all the networks that have low signal strength and informs the user
 of the proper actions.

*Optimization of 802.11 protocol
 -------------------------------
 Nowadays there are a number of 802.11 protocols (wi-fi) that are in use, a number of
 protocols that were in use and numerous protocols that will be in use in recent future.
 The decision making program finds all the wireless networks that do not use 802.11n
 protocol (the best option for now) and appear a proper message for user. There is also
 an advise for 802.11ac protocol which is gaining more and more ground.

*SYN flood occasion
 -----------------
 Final option of QoS section is 'SYN Pcket flood detection'. There is an included .csv
 file in the 'Files' folder, that is called attack.csv. Inside there are multiple SYN
 packets capable of pulling through a DOS (Denial-Of-Service) attack. In the Home Screen
 ('Load Data' screen) choose 'Wireshark' then 'Choose file', go to 'Files', choose
 attack.csv and 'Load to Database'. Then go to QoS section and choose the option 'SYN
 flag attack solution'.
 *** There is an extra tab 'Options' where you can select the SYN received packets that
     are allowed to be received in order to tackle a DOS attack of SYN flood type.

In the security section you can:

*Check for security existence
 ----------------------------
 The decision making program finds all the wirelles networks that do not use any security.
 There is a warning of using any kind of security (WEP, WPA) in order to secure a network.

*Optimize network security
 -------------------------
 The decision making program finds all the wirelles networks that do not use the best
 available security. A lot of netwroks administrators opt for WEP or WPA. However, the
 best option for the time being is WPA2-PSK using CCMP cipher (instead of TKIP). So the
 solution is to upgrade the security in an already secure network to WPA2.

An extra option in the 'Network Administration and Optimization' is the 'User' tab. In
this tab you can select your network's model router and you will be prompted to your router's
admin page. There you can use your username and password and make any changes you wish to.
Another extra tab is 'Options' in 'Network Administration and Optimization'. It serves as
number of packets handler for the 'SYN Pcket flood detection' in QoS. Also there is an
option for optimizing the only the network you are signed in or all of the networks that
you are detecting. Its purpose is beyond the exercise's scope.




Important notice
================

IN NO CASE YOU SHOULD MAKE ANY ALTERATIONS TO THE CODE PROVIDED WITH THE PROJECT,
OTHER THAN THE ONES REQUIRED TO RUN THE PROGRAM PROPERLY. ANY CHANGE YOU MAKE
MIGHT LEAD TO MALFUNCTION OF THE PROGRAM OR EVEN DISABILITY TO RUN. PLEASE
LEAVE THE CODE AS IS. IF YOU BELIEVE YOU FOUND SOMETHING WORTHMENTIONING PLEASE
FOLLOW THE INSTRUCTIONS IN THE UPCOMING SECTION 'CONTACT INFO'. IF YOU PROCEDE
TO ANY ALTERATIONS THE DEVELOPERS HAVE NO RESPONSIBILITY.




Important notice [2]
====================

Submitting the code as your own, referring to the code without the developers'
authorization or using the code without allowance is considered as a violation
of copyrights.




Contact info
============

For any problems that may occur, any comment that you have or any suggestion on
improving the user's experience, contact us at:
sdi1200030[at]di.uoa.gr
sdi1200110[at]di.uoa.gr